package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.model.dto.DiscountDto;
import com.example.shoppingcartwithspring.model.entity.DiscountEntity;
import com.example.shoppingcartwithspring.repository.DiscountRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;

import static com.example.shoppingcartwithspring.mock.DiscountMock.getDiscountDtoWithDiscountPercentageParametrized;
import static com.example.shoppingcartwithspring.mock.DiscountMock.getDiscountEntityWithPercentage;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DiscountServiceTest {

    @InjectMocks
    private DiscountService discountService;

    @Mock
    private DiscountRepository discountRepository;

    @Mock
    private ModelMapper mapper;

    @Test
    void givenAValidDiscountWhenCreateIsCalledTheCartIsSaved() {
        //GIVEN
        DiscountDto discount = getDiscountDtoWithDiscountPercentageParametrized(25);

        //WHEN
        when(discountRepository.save(any())).thenReturn(new DiscountEntity());
        when(mapper.map(any(DiscountDto.class), any(Class.class))).thenReturn(new DiscountEntity());
        when(mapper.map(any(DiscountEntity.class), any(Class.class))).thenReturn(discount);

        //THEN
        DiscountDto savedDiscount = discountService.create(discount);
        assertThat(savedDiscount).isNotNull();
    }

    @Test
    void findAllReturnsAllCreatedDiscounts() {
        //GIVEN
        DiscountEntity discountEntity1 = getDiscountEntityWithPercentage(20);
        DiscountEntity discountEntity2 = getDiscountEntityWithPercentage(30);
        DiscountDto discountDto1 = getDiscountDtoWithDiscountPercentageParametrized(20);
        DiscountDto discountDto2 = getDiscountDtoWithDiscountPercentageParametrized(30);

        //WHEN
        when(discountRepository.findAll()).thenReturn(List.of(discountEntity1, discountEntity2));
        when(mapper.map(discountEntity1, DiscountDto.class)).thenReturn(discountDto1);
        when(mapper.map(discountEntity2, DiscountDto.class)).thenReturn(discountDto2);


        //THEN
        List<DiscountDto> currentListOfDiscounts = discountService.findAll();
        assertThat(currentListOfDiscounts).hasSize(2);
        assertThat(currentListOfDiscounts.get(0).getDiscountPercentage()).isEqualTo(20);
        assertThat(currentListOfDiscounts.get(1).getDiscountPercentage()).isEqualTo(30);
    }

    @Test
    void findByNameReturnsAllExistingDiscountsWithSimilarName() {
        //GIVEN
        DiscountEntity discountEntity1 = getDiscountEntityWithPercentage(20);
        DiscountEntity discountEntity2 = getDiscountEntityWithPercentage(25);
        DiscountEntity discountEntity3 = getDiscountEntityWithPercentage(25);
        discountEntity3.setDescription("Christmas sale");
        DiscountDto discountDto1 = getDiscountDtoWithDiscountPercentageParametrized(20);
        DiscountDto discountDto2 = getDiscountDtoWithDiscountPercentageParametrized(25);

        //WHEN
        when(discountRepository.findAll()).thenReturn(List.of(discountEntity1, discountEntity2, discountEntity3));
        when(mapper.map(discountEntity1, DiscountDto.class)).thenReturn(discountDto1);
        when(mapper.map(discountEntity2, DiscountDto.class)).thenReturn(discountDto2);

        //THEN
        assertThat(discountService.findAll()).hasSize(3);
        List<DiscountDto> currentListsWithGivenDescription = discountService.findByDescription("Black Friday");
        assertThat(currentListsWithGivenDescription).hasSize(2);
        assertThat(currentListsWithGivenDescription.get(0).getDiscountPercentage()).isEqualTo(20);
        assertThat(currentListsWithGivenDescription.get(1).getDiscountPercentage()).isEqualTo(25);
    }
}
