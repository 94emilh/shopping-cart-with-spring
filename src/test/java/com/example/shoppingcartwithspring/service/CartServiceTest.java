package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.exception.NoCartWithGivenIdWasFoundException;
import com.example.shoppingcartwithspring.exception.NoItemWithGivenNameWasFoundException;
import com.example.shoppingcartwithspring.model.dto.CartDto;
import com.example.shoppingcartwithspring.model.dto.DiscountDto;
import com.example.shoppingcartwithspring.model.dto.ItemDto;
import com.example.shoppingcartwithspring.model.dto.ItemWrapperDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemCartEntity;
import com.example.shoppingcartwithspring.repository.CartRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NO_ITEM_WITH_GIVEN_NAME_WAS_FOUND;
import static com.example.shoppingcartwithspring.mock.DiscountMock.getDiscountDtoWithDiscountPercentageParametrized;
import static com.example.shoppingcartwithspring.mock.DiscountMock.getDiscountEntityWithPercentage;
import static com.example.shoppingcartwithspring.mock.ItemCartMock.getItemCartEntityWithItemAndQuantityParametrized;
import static com.example.shoppingcartwithspring.mock.ItemMock.APPLE_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.BANANA_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.getItemEntityWithName;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CartServiceTest {

    @InjectMocks
    private CartService cartService;

    @Mock
    private ItemService itemService;

    @Mock
    private CartRepository cartRepository;

    @Spy
    private ModelMapper mapper;

    @Test
    void givenAValidCartWhenCreateIsCalledTheCartIsSaved() {
        //GIVEN
        DiscountDto discount = getDiscountDtoWithDiscountPercentageParametrized(25);
        CartDto cartDto = new CartDto(discount);
        CartEntity cartEntity = new CartEntity(getDiscountEntityWithPercentage(25));

        //WHEN
        when(cartRepository.save(any())).thenReturn(cartEntity);
        when(mapper.map(cartEntity, CartDto.class)).thenReturn(cartDto);

        //THEN
        CartDto savedCart = cartService.create();
        assertThat(savedCart).isNotNull();
        assertThat(savedCart.getDiscount()).isEqualTo(discount);
    }

    @Test
    void findByIdReturnsAValidCartWhenGivenAValidCartId() {
        //GIVEN
        CartEntity cartEntity = new CartEntity(getDiscountEntityWithPercentage(10));
        cartEntity.setCartId(100);

        DiscountDto discountDto = getDiscountDtoWithDiscountPercentageParametrized(10);
        CartDto cartDto = new CartDto(discountDto);
        cartDto.setCartId(100);

        //WHEN
        when(cartRepository.findById(100L)).thenReturn(Optional.of(cartEntity));
        when(mapper.map(Optional.of(cartEntity), CartDto.class)).thenReturn(cartDto);

        //THEN
        CartDto byId = cartService.findById(100L);
        assertThat(byId).isNotNull();
        assertThat(byId.getDiscount()).isEqualTo(discountDto);
    }

    @Test
    void findByIdThrowsIdNotFoundExceptionWhenGivenAnInvalidId() {
        //WHEN
        when(cartRepository.findById(100L)).thenReturn(Optional.empty());

        //THEN
        assertThrows(NoCartWithGivenIdWasFoundException.class, () -> cartService.findById(100L));
    }


    @Test
    void findAllReturnsAllCreatedCarts() {
        //GIVEN
        CartEntity cartEntity1 = new CartEntity(getDiscountEntityWithPercentage(10));
        CartEntity cartEntity2 = new CartEntity(getDiscountEntityWithPercentage(20));
        DiscountDto discountDto1 = getDiscountDtoWithDiscountPercentageParametrized(10);
        DiscountDto discountDto2 = getDiscountDtoWithDiscountPercentageParametrized(20);

        //WHEN
        when(cartRepository.findAll()).thenReturn(List.of(cartEntity1, cartEntity2));
        when(mapper.map(cartEntity1, CartDto.class)).thenReturn(new CartDto(discountDto1));
        when(mapper.map(cartEntity2, CartDto.class)).thenReturn(new CartDto(discountDto2));

        //THEN
        List<CartDto> allSavedCarts = cartService.findAll();
        assertThat(allSavedCarts).isNotEmpty();
        assertThat(allSavedCarts.get(0).getDiscount()).isEqualTo(discountDto1);
        assertThat(allSavedCarts.get(1).getDiscount()).isEqualTo(discountDto2);
    }

    @Test
    void deleteItemByNameDeletesAllItemsWithGivenName() {
        //GIVEN
        CartEntity cartEntity = new CartEntity(getDiscountEntityWithPercentage(10));
        ItemCartEntity itemCartEntity1 = getItemCartEntityWithItemAndQuantityParametrized(getItemEntityWithName(BANANA_ITEM), 2);
        ItemCartEntity itemCartEntity2 = getItemCartEntityWithItemAndQuantityParametrized(getItemEntityWithName(APPLE_ITEM), 1);
        cartEntity.setItems(new HashSet<>(List.of(itemCartEntity1, itemCartEntity2)));

        //WHEN
        when(cartRepository.findById(cartEntity.getCartId())).thenReturn(Optional.of(cartEntity));
        when(cartRepository.findAll()).thenReturn(List.of(cartEntity));

        //THEN
        cartService.deleteItemByName(BANANA_ITEM, cartEntity.getCartId());
        assertThat(cartRepository.findAll().get(0).getItems()).hasSize(1);
    }

    @Test
    void testThatAddItemInCartActuallyAddsAnItemInCartWhenGivenExistingItem() {
        //GIVEN
        CartEntity cartEntity = new CartEntity();
        cartEntity.setCartId(100L);
        ItemWrapperDto itemWrapperDto = new ItemWrapperDto();
        itemWrapperDto.setName(BANANA_ITEM);
        itemWrapperDto.setQuantity(2);

        //WHEN
        when(cartRepository.findById(100L)).thenReturn(Optional.of(cartEntity));
        when(itemService.findItemByName(BANANA_ITEM)).thenReturn(new ItemDto(BANANA_ITEM, 10));
        when(itemService.itemAlreadyExists(BANANA_ITEM)).thenReturn(true);
        cartService.addItemInCart(itemWrapperDto, 100L);

        //THEN
        List<ItemCartEntity> listOfItems = new ArrayList<>(cartEntity.getItems());
        assertThat(listOfItems.size()).isEqualTo(1);
        assertThat(listOfItems.get(0).getQuantity()).isEqualTo(2);
    }

    @Test
    void testThatAddItemInCartThrowsExceptionWhenGivenNonExistingItem() {
        //GIVEN
        CartEntity cartEntity = new CartEntity();
        cartEntity.setCartId(100L);
        ItemWrapperDto itemWrapperDto = new ItemWrapperDto();
        itemWrapperDto.setName(BANANA_ITEM);
        itemWrapperDto.setQuantity(2);

        //WHEN
        when(cartRepository.findById(100L)).thenReturn(Optional.of(cartEntity));
        when(itemService.itemAlreadyExists(BANANA_ITEM)).thenReturn(false);

        //THEN
        NoItemWithGivenNameWasFoundException thrownException = assertThrows(NoItemWithGivenNameWasFoundException.class,
                                                                            () -> cartService.addItemInCart(itemWrapperDto,
                                                                                                            100L));
        assertThat(thrownException.getMessage()).contains(NO_ITEM_WITH_GIVEN_NAME_WAS_FOUND.getMessageWithParams(BANANA_ITEM));
    }
}
