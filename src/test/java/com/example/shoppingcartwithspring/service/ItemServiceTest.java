package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.exception.NoItemWithGivenIdWasFoundException;
import com.example.shoppingcartwithspring.model.dto.ItemDto;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;
import com.example.shoppingcartwithspring.repository.ItemRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.Optional;

import static com.example.shoppingcartwithspring.mock.ItemMock.APPLE_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.BANANA_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.getItemDtoWithName;
import static com.example.shoppingcartwithspring.mock.ItemMock.getItemEntityWithName;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ItemServiceTest {
    @InjectMocks
    private ItemService itemService;

    @Mock
    private ItemRepository itemRepository;

    @Mock
    private ModelMapper mapper;

    @Test
    void createItemWorksAsExpected() {
        //GIVEN
        ItemDto itemToSave = getItemDtoWithName(BANANA_ITEM);
        ItemEntity itemEntity = getItemEntityWithName(BANANA_ITEM);
        //WHEN
        when(itemRepository.save(any(ItemEntity.class))).thenReturn(itemEntity);
        when(mapper.map(any(ItemEntity.class), any(Class.class))).thenReturn(itemToSave);
        when(mapper.map(any(ItemDto.class), any(Class.class))).thenReturn(itemEntity);

        //THEN
        ItemDto savedItem = itemService.create(itemToSave);
        assertThat(savedItem).isNotNull();
        assertThat(savedItem.getName()).isEqualTo(itemToSave.getName());
        assertThat(savedItem.getPrice()).isEqualTo(itemToSave.getPrice());
    }

    @Test
    void updatePriceActuallyUpdatesThePrice() {
        //WHEN
        when(itemRepository.findAll()).thenReturn(List.of(getItemEntityWithName(BANANA_ITEM)));
        when(itemService.findItemByName(BANANA_ITEM)).thenReturn(getItemDtoWithName(BANANA_ITEM));

        //THEN
        ItemDto itemWithUpdatedPrice = itemService.updatePriceForGivenItem(BANANA_ITEM, 15);
        assertThat(itemWithUpdatedPrice.getPrice()).isEqualTo(15);
    }

    @Test
    void findByIdReturnsAValidItemWhenGivenAValidItemId() {
        //GIVEN
        ItemDto itemDtoToSave = getItemDtoWithName(BANANA_ITEM);
        itemDtoToSave.setId(100);
        ItemEntity itemEntityToSave = getItemEntityWithName(BANANA_ITEM);
        itemEntityToSave.setId(100);

        //WHEN
        when(itemRepository.findById(100L)).thenReturn(Optional.of(itemEntityToSave));
        when(mapper.map(any(Optional.class), any(Class.class))).thenReturn(itemDtoToSave);

        //THEN
        ItemDto byId = itemService.findById(100L);
        assertFindByIdItem(byId);
    }

    @Test
    void findByIdThrowsIdNotFoundExceptionWhenGivenAnInvalidId() {
        //WHEN
        when(itemRepository.findById(100L)).thenReturn(Optional.empty());

        //THEN
        assertThrows(NoItemWithGivenIdWasFoundException.class, () -> itemService.findById(100L));
    }

    @Test
    void findAllReturnsAllCreatedItems() {
        //GIVEN
        ItemEntity itemEntityToSave1 = getItemEntityWithName(BANANA_ITEM);
        ItemEntity itemEntityToSave2 = getItemEntityWithName(APPLE_ITEM);

        //WHEN
        when(itemRepository.findAll()).thenReturn(List.of(itemEntityToSave1, itemEntityToSave2));
        when(mapper.map(itemEntityToSave1, ItemDto.class)).thenReturn(getItemDtoWithName(BANANA_ITEM));
        when(mapper.map(itemEntityToSave2, ItemDto.class)).thenReturn(getItemDtoWithName(APPLE_ITEM));

        //THEN
        List<ItemDto> allSavedItems = itemService.findAll();
        assertThat(allSavedItems).isNotEmpty();
        assertThat(allSavedItems.size()).isEqualTo(2);
    }

    private void assertFindByIdItem(ItemDto byId) {
        assertThat(byId).isNotNull();
        assertThat(byId.getName()).isEqualTo(BANANA_ITEM);
        assertThat(byId.getPrice()).isEqualTo(10);
    }
}
