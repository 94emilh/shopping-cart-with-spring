package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.model.dto.CartDto;
import com.example.shoppingcartwithspring.model.dto.ItemCartDto;
import com.example.shoppingcartwithspring.model.dto.OrderDto;
import com.example.shoppingcartwithspring.model.dto.request.CreateOrderRequestDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemCartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;
import com.example.shoppingcartwithspring.model.entity.OrderEntity;
import com.example.shoppingcartwithspring.repository.ItemCartRepository;
import com.example.shoppingcartwithspring.repository.OrderRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class OrderServiceTest {

    @InjectMocks
    private OrderService orderService;
    @Mock
    private CartService cartService;

    @Mock
    private ItemCartRepository itemCartRepository;

    @Mock
    private OrderRepository orderRepository;

    @Mock
    private ModelMapper mapper;

    @Test
    void testCreateOrderSuccessfully() {
        // GIVEN
        Long cartId = 1L;
        String userId = "user123";

        ItemCartDto itemCartDto1 = new ItemCartDto(1L, 2);
        ItemCartDto itemCartDto2 = new ItemCartDto(2L, 3);

        OrderDto expectedOrderDto = new OrderDto();
        expectedOrderDto.setOrderId(100L);
        expectedOrderDto.setItems(Set.of(itemCartDto1, itemCartDto2));
        expectedOrderDto.setTotalPrice(23.0);
        expectedOrderDto.setOrderStatus("Pending");
        expectedOrderDto.setUserId(userId);

        CartDto cartDto = new CartDto();
        cartDto.setCartId(cartId);
        cartDto.setItems(Set.of(itemCartDto1, itemCartDto2));
        cartDto.setTotalAmountToPay(23.0);

        ItemCartEntity itemCartEntity1 = new ItemCartEntity();
        itemCartEntity1.setId(10L);
        itemCartEntity1.setQuantity(2);
        itemCartEntity1.setItem(new ItemEntity("Banana", 5.0));

        ItemCartEntity itemCartEntity2 = new ItemCartEntity();
        itemCartEntity2.setId(11L);
        itemCartEntity2.setQuantity(3);
        itemCartEntity2.setItem(new ItemEntity("Apple", 3.0));

        CartEntity cartEntity = new CartEntity();
        cartEntity.setCartId(cartId);
        cartEntity.setItems(new HashSet<>(Set.of(itemCartEntity1, itemCartEntity2)));
        cartEntity.setTotalAmountToPay(23.0);

        when(mapper.map(cartDto, CartEntity.class)).thenReturn(cartEntity);
        when(cartService.findById(cartId)).thenReturn(cartDto);
        when(itemCartRepository.findByItem_Id(anyLong())).thenReturn(Optional.of(itemCartEntity1), Optional.of(itemCartEntity2));

        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setItems(Set.of(itemCartEntity1, itemCartEntity2));
        orderEntity.setTotalPrice(23.0);
        orderEntity.setOrderStatus("Pending");
        orderEntity.setUserId(userId);

        when(orderRepository.save(Mockito.any())).thenReturn(orderEntity);
        when(mapper.map(Mockito.any(OrderEntity.class), eq(OrderDto.class))).thenReturn(expectedOrderDto);

        // WHEN
        OrderDto actualOrderDto = orderService.createOrder(new CreateOrderRequestDto(cartId, userId));

        // THEN
        assertThat(actualOrderDto).isNotNull();
        assertThat(actualOrderDto.getOrderStatus()).isEqualTo("Pending");
        assertThat(actualOrderDto.getTotalPrice()).isEqualTo(23.0);
        assertThat(actualOrderDto.getItems()).hasSize(2);
        assertThat(actualOrderDto.getUserId()).isEqualTo(userId);
        verify(orderRepository, times(1)).save(orderEntity);
    }

}
