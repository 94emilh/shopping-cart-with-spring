package com.example.shoppingcartwithspring.integration;

import com.example.shoppingcartwithspring.audit.repository.AuditRepository;
import com.example.shoppingcartwithspring.model.dto.request.ItemRequestDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;
import com.example.shoppingcartwithspring.repository.CartRepository;
import com.example.shoppingcartwithspring.repository.ItemCartRepository;
import com.example.shoppingcartwithspring.repository.ItemRepository;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.Arguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.stream.Stream;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.ITEM_NAME_ALREADY_EXISTS;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NAME_NOT_BLANK_OR_NULL;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.PRICE_NOT_NEGATIVE_OR_NULL;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.PRICE_SHOULD_BE_A_NUMBER;
import static com.example.shoppingcartwithspring.mock.DiscountMock.getDiscountEntityWithPercentage;
import static com.example.shoppingcartwithspring.mock.ItemMock.BANANA_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.BEER_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.getItemEntityWithName;
import static io.restassured.RestAssured.given;
import static java.lang.String.valueOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

class ItemControllerIT extends BaseIT {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private AuditRepository auditRepository;

    @Autowired
    private ItemCartRepository itemCartRepository;

    @LocalServerPort
    private int port;

    @BeforeEach
    void init() {
        cartRepository.deleteAll();
        itemRepository.deleteAll();
        auditRepository.deleteAll();
        itemCartRepository.deleteAll();
    }

    private static Stream<Arguments> isAdminStatusCodeCombination() {
        return Stream.of(Arguments.of(true, OK),
                         Arguments.of(false, FORBIDDEN));
    }

    @Test
    void whenCreatingANewItemItActuallyCreatesItIfCalledByNormalUser() {
        //WHEN
        postForCreatingItem(BANANA_ITEM, OK.value(), adminAuthorizationHeader);

        //THEN
        assertThat(itemRepository.findAll()).hasSize(1);
        ItemEntity savedItem = itemRepository.findAll().get(0);
        assertThat(savedItem.getName()).isEqualTo(BANANA_ITEM);
        assertThat(savedItem.getPrice()).isEqualTo(10);
    }

    @Test
    void whenCreatingANewItemReturns403ForbiddenIfCalledByAdmin() {
        //WHEN
        ItemRequestDto itemRequestDto = new ItemRequestDto();
        itemRequestDto.setName(BANANA_ITEM);
        itemRequestDto.setPrice(valueOf(10));


        //THEN
        given().contentType(APPLICATION_JSON)
                      .headers(userAuthorizationHeader)
                      .body(itemRequestDto)
                      .expect()
                      .statusCode(FORBIDDEN.value())
                      .when()
                      .post(BASE_PATH + port + "/item/create");
    }

    @Test
    void whenCreatingAnItemAndAddingItToACartTheItemIsActuallyAddedInTheCart() {
        CartEntity cart = new CartEntity(getDiscountEntityWithPercentage(20));
        cartRepository.save(cart);

        itemRepository.save(getItemEntityWithName(BEER_ITEM));

        postForAddingItemInCart(BEER_ITEM, "3", valueOf(cart.getCartId()));

        assertThat(itemRepository.findAll()).hasSize(1);
        assertThat(itemCartRepository.findAll()).hasSize(1);
        assertThat(cartRepository.findAll().get(0).getItems().size()).isEqualTo(1);
        assertThat(auditRepository.findAll().size()).isEqualTo(1);
    }

    @Test
    void creatingAnItemTwiceThrowsException() {
        postForCreatingItem(BANANA_ITEM, OK.value(), adminAuthorizationHeader);
        Response response = postForCreatingItem(BANANA_ITEM, INTERNAL_SERVER_ERROR.value(), adminAuthorizationHeader);
        assertThat(response.getBody().asString()).contains(ITEM_NAME_ALREADY_EXISTS.getMessageWithParams(BANANA_ITEM));
    }

    @Test
    void creatingAnItemWithNegativePriceThrowsValidationException() {
        Response response = postForCreatingItemWithNamePriceAndStatusCodeParametrized(BANANA_ITEM, valueOf(-10), BAD_REQUEST.value());
        assertThat(response.getBody().asString()).contains(PRICE_NOT_NEGATIVE_OR_NULL.getMessage());
    }

    @Test
    void creatingAnItemWithBlankNameThrowsValidationException() {
        Response response = postForCreatingItem("", BAD_REQUEST.value(), adminAuthorizationHeader);
        assertThat(response.getBody().asString()).contains(NAME_NOT_BLANK_OR_NULL.getMessage());
    }

    @Test
    void creatingAnItemWithInvalidPriceThrowsValidationException() {
        Response response = postForCreatingItemWithNamePriceAndStatusCodeParametrized(BANANA_ITEM,
                                                                                      "invalidPrice", BAD_REQUEST.value());
        assertThat(response.getBody().asString()).contains(PRICE_SHOULD_BE_A_NUMBER.getMessage());
    }

    @Test
    void findByNameReturnsItemInCaseItExists() {
        postForCreatingItem(BANANA_ITEM, OK.value(), adminAuthorizationHeader);

        //THEN
        Response response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .queryParam("itemName", BANANA_ITEM)
                .expect()
                .statusCode(200)
                .when()
                .get(BASE_PATH + port + "/item/getByName");

        assertThat(response.getBody().asString()).contains(BANANA_ITEM);
    }

    @Test
    void updatePriceActuallyUpdatesTheItemPriceIfCalledByAdmin() {
        //GIVEN
        postForCreatingItem(BANANA_ITEM, 200, adminAuthorizationHeader);

        //THEN
        given().contentType(APPLICATION_JSON)
               .headers(adminAuthorizationHeader)
               .queryParam("price", 15)
               .expect()
               .statusCode(OK.value())
               .when()
               .patch(BASE_PATH + port + "/item/updateprice/banana");
    }

    @Test
    void updatePriceReturns403ForbiddenWhenCalledByNormalUser() {
        //GIVEN
        postForCreatingItem(BANANA_ITEM, 200, adminAuthorizationHeader);

        //THEN
        given().contentType(APPLICATION_JSON)
               .headers(userAuthorizationHeader)
               .queryParam("price", 15)
               .expect()
               .statusCode(FORBIDDEN.value())
               .when()
               .patch(BASE_PATH + port + "/item/updateprice/banana");
    }

    private Response postForCreatingItem(String name,
                                         int statusCode,
                                         Headers headers) {
        ItemRequestDto itemRequestDto = new ItemRequestDto();
        itemRequestDto.setName(name);
        itemRequestDto.setPrice(valueOf(10));
        return given().contentType(APPLICATION_JSON)
                      .headers(headers)
                      .body(itemRequestDto)
                      .expect()
                      .statusCode(statusCode)
                      .when()
                      .post(BASE_PATH + port + "/item/create");
    }

    private Response postForCreatingItemWithNamePriceAndStatusCodeParametrized(String name,
                                                                               String price,
                                                                               int statusCode) {

        ItemRequestDto itemRequestDto = new ItemRequestDto();
        itemRequestDto.setName(name);
        itemRequestDto.setPrice(price);
        return given().contentType(APPLICATION_JSON)
                      .headers(adminAuthorizationHeader)
                      .body(itemRequestDto)
                      .expect()
                      .statusCode(statusCode)
                      .when()
                      .post(BASE_PATH + port + "/item/create");
    }
}
