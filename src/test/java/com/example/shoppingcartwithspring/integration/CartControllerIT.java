package com.example.shoppingcartwithspring.integration;

import com.example.shoppingcartwithspring.audit.repository.AuditRepository;
import com.example.shoppingcartwithspring.model.dto.request.ItemWrapperRequestDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemCartEntity;
import com.example.shoppingcartwithspring.repository.CartRepository;
import com.example.shoppingcartwithspring.repository.DiscountRepository;
import com.example.shoppingcartwithspring.repository.ItemCartRepository;
import com.example.shoppingcartwithspring.repository.ItemRepository;
import com.example.shoppingcartwithspring.service.CartService;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.ArrayList;
import java.util.List;

import static com.example.shoppingcartwithspring.audit.model.ActionType.ADD_ITEM_TO_CART;
import static com.example.shoppingcartwithspring.audit.model.ActionType.INITIATE_CART;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NO_ITEM_WITH_GIVEN_NAME_WAS_FOUND;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.QUANTITY_NOT_NEGATIVE_OR_NULL;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.QUANTITY_SHOULD_BE_A_NUMBER;
import static com.example.shoppingcartwithspring.mock.DiscountMock.getDiscountEntityWithPercentage;
import static com.example.shoppingcartwithspring.mock.ItemMock.APPLE_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.BANANA_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.BEER_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.getItemEntityWithNameAndPrice;
import static com.example.shoppingcartwithspring.mock.ItemMock.getItemEntityWithName;
import static io.restassured.RestAssured.given;
import static java.lang.String.valueOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

class CartControllerIT extends BaseIT {

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ItemCartRepository itemCartRepository;
    @Autowired
    private DiscountRepository discountRepository;

    @Autowired
    private AuditRepository auditRepository;

    @Autowired
    private CartService cartService;

    @LocalServerPort
    private int port;

    @BeforeEach
    void init() {
        cartRepository.deleteAll();
        itemCartRepository.deleteAll();
        itemRepository.deleteAll();
        auditRepository.deleteAll();
        discountRepository.deleteAll();
    }

    @Test
    void creatingACartAddsItInTheListOfCarts() {
        Response response = postForCreatingCart(20);

        String responseBody = response.getBody().asString();
        assertThat(responseBody).contains("\"discountPercentage\":20.0");
        assertThat(cartRepository.findAll()).hasSize(1);
        assertThat(cartRepository.findAll().get(0).getDiscount().getDiscountPercentage()).isEqualTo(20);
        assertThat(auditRepository.findAll().size()).isEqualTo(1);
        assertThat(auditRepository.findAll().get(0).getActionType()).isEqualTo(INITIATE_CART);
    }

    @Test
    void findAllCartsReturnsAllExistingCarts() {
        postForCreatingCart(20);
        postForCreatingCart(30);

        assertThat(cartRepository.findAll()).hasSize(2);
        assertThat(cartRepository.findAll().get(0).getDiscount().getDiscountPercentage()).isEqualTo(20);
        assertThat(cartRepository.findAll().get(1).getDiscount().getDiscountPercentage()).isEqualTo(30);
        assertThat(auditRepository.findAll().size()).isEqualTo(2);
    }

    @Test
    void addingAnItemToCartWorksAsExpected() {
        CartEntity cart = new CartEntity(getDiscountEntityWithPercentage(20));
        cartRepository.save(cart);
        itemRepository.save(getItemEntityWithName(BANANA_ITEM));

        postForAddingItemInCart(BANANA_ITEM, "1", valueOf(cart.getCartId()));

        List<ItemCartEntity> listOfItems = new ArrayList<>(cartRepository.findAll().get(0).getItems());
        assertThat(listOfItems.size()).isEqualTo(1);
        assertThat(listOfItems.get(0).getItem().getName()).isEqualTo(BANANA_ITEM);
        assertThat(auditRepository.findAll().size()).isEqualTo(1);
        assertThat(auditRepository.findAll().get(0).getActionType()).isEqualTo(ADD_ITEM_TO_CART);
    }

    @Test
    void addingAnItemWithNegativeQuantityToCartThrowsValidationException() {
        CartEntity cart = new CartEntity();
        cartRepository.save(cart);
        itemRepository.save(getItemEntityWithName(BANANA_ITEM));

        ItemWrapperRequestDto itemWrapperRequestDto = new ItemWrapperRequestDto();
        itemWrapperRequestDto.setName(BANANA_ITEM);
        itemWrapperRequestDto.setQuantity("-10");

        Response response = postForAddingItemInCartWithBadRequest(cart, itemWrapperRequestDto);

        assertThat(response.getBody().asString()).contains(QUANTITY_NOT_NEGATIVE_OR_NULL.getMessage());
    }

    @Test
    void addingAnItemWithNonNumberQuantityToCartThrowsValidationException() {
        CartEntity cart = new CartEntity();
        cartRepository.save(cart);
        itemRepository.save(getItemEntityWithName(BANANA_ITEM));

        ItemWrapperRequestDto itemWrapperRequestDto = new ItemWrapperRequestDto();
        itemWrapperRequestDto.setName(BANANA_ITEM);
        itemWrapperRequestDto.setQuantity("non-number");

        Response response = postForAddingItemInCartWithBadRequest(cart, itemWrapperRequestDto);

        assertThat(response.getBody().asString()).contains(QUANTITY_SHOULD_BE_A_NUMBER.getMessage());
    }

    @Test
    void addingAnItemInCartWhichDoesNotExistInListOfItemsThrowsException() {
        CartEntity cart = new CartEntity(getDiscountEntityWithPercentage(20));
        cartRepository.save(cart);
        itemRepository.save(getItemEntityWithName(BANANA_ITEM));

        ItemWrapperRequestDto itemWrapperRequestDto = new ItemWrapperRequestDto();
        itemWrapperRequestDto.setName(APPLE_ITEM);
        itemWrapperRequestDto.setQuantity("1");

        Response response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body(itemWrapperRequestDto)
                .expect()
                .statusCode(INTERNAL_SERVER_ERROR.value())
                .when()
                .post(BASE_PATH + port + "/cart/" + cart.getCartId() + "/additem");

        assertThat(response.getBody().asString()).contains(NO_ITEM_WITH_GIVEN_NAME_WAS_FOUND.getMessageWithParams(APPLE_ITEM));
    }

    @Test
    void deleteCartByIdWorksAsExpectedIfCalledByAdmin() {
        //WHEN
        CartEntity cart = new CartEntity(getDiscountEntityWithPercentage(20));
        cartRepository.save(cart);
        assertThat(cartService.findAll()).hasSize(1);

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .expect()
                .statusCode(OK.value())
                .when()
                .delete(BASE_PATH + port + "/cart/" + cart.getCartId());
            assertThat(cartService.findAll()).isEmpty();
    }

    @Test
    void deleteCartByIdReturns403ForbiddenIfCalledByNormalUser() {
        //WHEN
        CartEntity cart = new CartEntity(getDiscountEntityWithPercentage(20));
        cartRepository.save(cart);
        assertThat(cartService.findAll()).hasSize(1);

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(userAuthorizationHeader)
                .expect()
                .statusCode(FORBIDDEN.value())
                .when()
                .delete(BASE_PATH + port + "/cart/" + cart.getCartId());
    }

    @Test
    void deleteItemsByNameWorksAsExpectedIfCalledByAdmin() {
        //WHEN
        long cartId = createCartAndAddItemsToItAndReturnCartId();

        //THEN
        given().contentType(APPLICATION_JSON)
               .headers(adminAuthorizationHeader)
               .queryParam("itemName", APPLE_ITEM)
               .expect()
               .statusCode(OK.value())
               .when()
               .patch(BASE_PATH + port + "/cart/deleteitems/" + cartId);
            assertThat(new ArrayList<>(cartRepository.findAll()).get(0).getItems()).hasSize(2);
    }

    @Test
    void deleteItemsByNameReturns403ForbiddenIfCalledByNormalUser() {
        //WHEN
        long cartId = createCartAndAddItemsToItAndReturnCartId();

        //THEN
        given().contentType(APPLICATION_JSON)
               .headers(userAuthorizationHeader)
               .queryParam("itemName", APPLE_ITEM)
               .expect()
               .statusCode(FORBIDDEN.value())
               .when()
               .patch(BASE_PATH + port + "/cart/deleteitems/" + cartId);
    }

    private long createCartAndAddItemsToItAndReturnCartId() {
        CartEntity cart = new CartEntity(getDiscountEntityWithPercentage(20));
        cartRepository.save(cart);
        itemRepository.save(getItemEntityWithNameAndPrice(BANANA_ITEM, 5));
        itemRepository.save(getItemEntityWithNameAndPrice(APPLE_ITEM, 2));
        itemRepository.save(getItemEntityWithNameAndPrice(BEER_ITEM, 10));

        postForAddingItemInCart(BANANA_ITEM, "2", valueOf(cart.getCartId()));
        postForAddingItemInCart(APPLE_ITEM, "3", valueOf(cart.getCartId()));
        postForAddingItemInCart(BEER_ITEM, "5", valueOf(cart.getCartId()));
        return cart.getCartId();
    }

    private Response postForAddingItemInCartWithBadRequest(CartEntity cart,
                                                           ItemWrapperRequestDto itemWrapperRequestDto) {
        return given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body(itemWrapperRequestDto)
                .expect()
                .statusCode(BAD_REQUEST.value())
                .when()
                .post(BASE_PATH + port + "/cart/" + cart.getCartId() + "/additem");
    }

    private Response postForCreatingCart(int discountPercentage) {
        return given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body("{\"discount\": {\"discountPercentage\" :" + discountPercentage + "}}")
                .expect()
                .statusCode(OK.value())
                .when()
                .post(BASE_PATH + port + "/cart/create");
    }
}
