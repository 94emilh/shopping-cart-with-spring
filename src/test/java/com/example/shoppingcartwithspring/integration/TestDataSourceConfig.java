package com.example.shoppingcartwithspring.integration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

import static com.example.shoppingcartwithspring.integration.BaseIT.pgContainer;

@Configuration
public class TestDataSourceConfig {

    @Value("${spring.datasource.username}")
    private String username;

    @Value("${spring.datasource.password}")
    private String password;
    @Bean
    public DataSource dataSource() {
        return DataSourceBuilder.create()
                                .url(pgContainer.getJdbcUrl())
                                .username(username)
                                .password(password)
                                .build();
    }
}
