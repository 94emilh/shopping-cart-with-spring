package com.example.shoppingcartwithspring.integration;

import org.json.JSONException;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.web.server.LocalServerPort;

import static io.restassured.RestAssured.given;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.OK;

class UserControllerIT extends BaseIT {
    public static final String API_USERS_ENDPOINT = "/api/users";

    @LocalServerPort
    private int port;

    @Test
    void testGetWithTokenReturns200StatusCode() throws JSONException {
        given()
                .headers(adminAuthorizationHeader)
                .when()
                .get(BASE_PATH + port + API_USERS_ENDPOINT)
                .then()
                .assertThat()
                .statusCode(OK.value());
    }

    @Test
    void testGetWithoutTokenReturns403StatusCode() {
        given().when()
                .get(BASE_PATH + port + API_USERS_ENDPOINT)
                .then()
                .assertThat()
                .statusCode(FORBIDDEN.value());
    }
}
