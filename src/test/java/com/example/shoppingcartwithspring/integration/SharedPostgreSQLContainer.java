package com.example.shoppingcartwithspring.integration;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

import static com.example.shoppingcartwithspring.integration.BaseIT.SHARED_NETWORK;


public class SharedPostgreSQLContainer extends PostgreSQLContainer<SharedPostgreSQLContainer> {

    private static final DockerImageName POSTGRES_IMAGE = DockerImageName.parse("postgres:15.2-alpine")
                                                                         .asCompatibleSubstituteFor("postgres");
    private static final Integer DEFAULT_POSTGRES_PORT = 5432;

    public static PostgreSQLContainer<?> pgContainer;

    private SharedPostgreSQLContainer() {
        super(POSTGRES_IMAGE);
    }

    @SuppressWarnings("all")
    public static PostgreSQLContainer<?> createContainer() {
        try (PostgreSQLContainer<?> container = new SharedPostgreSQLContainer().withNetwork(SHARED_NETWORK)
                                                                               .withNetworkMode(SHARED_NETWORK.getId())
                                                                               .withExposedPorts(DEFAULT_POSTGRES_PORT)
                                                                               .withDatabaseName("shoppingcartwithspring")
                                                                               .withUsername("postgres")
                                                                               .withPassword("postgres")) {
            pgContainer = container;
        }
        return pgContainer;
    }

    @Override
    public void start() {
        super.start();
    }

    @Override
    public void stop() {
        // do nothing, as we let JVM to handle the shutdown
    }

    public static class Initializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {
        @Override
        public void initialize(ConfigurableApplicationContext applicationContext) {
             TestPropertySourceUtils.addInlinedPropertiesToEnvironment(applicationContext, "spring.datasource.url=" + pgContainer.getJdbcUrl());
        }
    }
}
