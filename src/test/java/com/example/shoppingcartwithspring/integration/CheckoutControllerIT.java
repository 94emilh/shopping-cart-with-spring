package com.example.shoppingcartwithspring.integration;

import com.example.shoppingcartwithspring.audit.model.entity.AuditEntity;
import com.example.shoppingcartwithspring.audit.repository.AuditRepository;
import com.example.shoppingcartwithspring.model.dto.request.CheckoutRequestDto;
import com.example.shoppingcartwithspring.model.dto.response.CheckoutResponseDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.repository.CartRepository;
import com.example.shoppingcartwithspring.repository.ItemCartRepository;
import com.example.shoppingcartwithspring.repository.ItemRepository;
import com.example.shoppingcartwithspring.repository.OrderRepository;
import io.restassured.response.Response;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.List;

import static com.example.shoppingcartwithspring.audit.model.ActionType.ADD_ITEM_TO_CART;
import static com.example.shoppingcartwithspring.audit.model.ActionType.CHECKOUT;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.USER_ID_DOES_NOT_MATCH_WITH_AUTHENTICATED_USER;
import static com.example.shoppingcartwithspring.mock.DiscountMock.getDiscountEntityWithPercentage;
import static com.example.shoppingcartwithspring.mock.ItemMock.APPLE_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.BANANA_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.BEER_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.getItemEntityWithNameAndPrice;
import static io.restassured.RestAssured.given;
import static java.lang.String.valueOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.*;

class CheckoutControllerIT extends BaseIT{

    @Autowired
    private AuditRepository auditRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ItemCartRepository itemCartRepository;

    @Autowired
    private OrderRepository orderRepository;

    @LocalServerPort
    private int port;

    @BeforeEach
    void init() {
        orderRepository.deleteAll();
        cartRepository.deleteAll();
        itemCartRepository.deleteAll();
        itemRepository.deleteAll();
        auditRepository.deleteAll();
    }

    @AfterAll
    public void cleanUp() {
        orderRepository.deleteAll();
    }
    @Test
    void checkOutIsDisplayedProperlyWhenCalledBySameUserIdAsTheOneFromTheToken() {
        //GIVEN
        CheckoutRequestDto requestDto = new CheckoutRequestDto(createCartAndAddItemsToItAndReturnCartId(), ADMIN_USERNAME);

        //WHEN
        CheckoutResponseDto response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body(requestDto)
                .expect()
                .statusCode(OK.value())
                .when()
                .post(BASE_PATH + port + "/checkout")
                .as(CheckoutResponseDto.class);

        //THEN
        assertThat(response.receipt()).contains("Total before discount: 66");
        assertThat(response.receipt()).contains("Total after discount: 52.8");
        assertThat(response.order()).isNotNull();
        assertThat(response.order().getTotalPrice()).isEqualTo(52.8);
        assertThat(response.order().getItems()).isNotEmpty();
        List<AuditEntity> audits = auditRepository.findAll();
        assertThat(audits.size()).isEqualTo(4);
        assertThat(audits.stream().map(AuditEntity::getActionType)).containsExactlyInAnyOrder(
                ADD_ITEM_TO_CART,
                ADD_ITEM_TO_CART,
                ADD_ITEM_TO_CART,
                CHECKOUT
        );
    }

    @Test
    void checkOutReturnsForbiddenIfCalledByDifferentUserIdComparedWithTheOneFromTheToken() {
        //GIVEN
        CheckoutRequestDto requestDto = new CheckoutRequestDto(createCartAndAddItemsToItAndReturnCartId(), USER_USERNAME);

        //WHEN
        Response response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body(requestDto)
                .expect()
                .statusCode(FORBIDDEN.value())
                .when()
                .post(BASE_PATH + port + "/checkout");
        assertThat(response.getBody().asString()).contains(USER_ID_DOES_NOT_MATCH_WITH_AUTHENTICATED_USER.getMessage());
    }

    private long createCartAndAddItemsToItAndReturnCartId() {
        CartEntity cart = new CartEntity(getDiscountEntityWithPercentage(20));
        cartRepository.save(cart);
        itemRepository.save(getItemEntityWithNameAndPrice(BANANA_ITEM, 5));
        itemRepository.save(getItemEntityWithNameAndPrice(APPLE_ITEM, 2));
        itemRepository.save(getItemEntityWithNameAndPrice(BEER_ITEM, 10));
        itemRepository.flush();

        postForAddingItemInCart(BANANA_ITEM, "2", valueOf(cart.getCartId()));
        postForAddingItemInCart(APPLE_ITEM, "3", valueOf(cart.getCartId()));
        postForAddingItemInCart(BEER_ITEM, "5", valueOf(cart.getCartId()));
        return cart.getCartId();
    }
}
