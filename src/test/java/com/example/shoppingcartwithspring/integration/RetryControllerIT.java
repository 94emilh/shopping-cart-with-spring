package com.example.shoppingcartwithspring.integration;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.read.ListAppender;
import com.example.shoppingcartwithspring.service.RetryService;
import io.restassured.response.Response;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class RetryControllerIT extends BaseIT {

    @LocalServerPort
    private int port;

    private ListAppender<ILoggingEvent> listAppender;

    @BeforeEach
    @SneakyThrows
    void init() {
        Logger logger = (Logger) LoggerFactory.getLogger(RetryService.class);
        listAppender = new ListAppender<>();
        listAppender.start();
        logger.addAppender(listAppender);
    }

    @Test
    void retryRteControllerIsCalled3TimesBeforeThrowingInternalServerError() {
        given().contentType(APPLICATION_JSON)
                .expect()
                .statusCode(SERVICE_UNAVAILABLE.value())
                .when()
                .get(BASE_PATH + port + "/retry/rte");
        List<ILoggingEvent> logsList = listAppender.list;
        assertThat(logsList).isNotEmpty();
        assertThat(logsList).hasSize(3);
        List<Level> listOfLogLevels = extractTriggeredLogsList(logsList);
        assertThat(listOfLogLevels).hasSize(1);
        assertThat(listOfLogLevels.get(0)).isEqualTo(Level.INFO);
    }

    @Test
    void retryWithHttpStatusParameterizedIsCalled3TimesWhenReceivingProperExceptionAsParameter(){
        Response response = given().contentType(APPLICATION_JSON)
                                     .params(Map.of("httpStatus", "500"))
                                     .expect()
                                     .statusCode(SERVICE_UNAVAILABLE.value())
                                     .when()
                                     .get(BASE_PATH + port + "/retry/statuscodeexception");

        assertThat(response.getBody().asString()).contains("Please try after 10 minutes!");

        List<ILoggingEvent> logsList = listAppender.list;
        assertThat(logsList).isNotEmpty();
        assertThat(logsList).hasSize(3);
        List<Level> listOfLogLevels = extractTriggeredLogsList(logsList);
        assertThat(listOfLogLevels).hasSize(1);
        assertThat(listOfLogLevels.get(0)).isEqualTo(Level.INFO);
    }

    @Test
    void retryWithHttpStatusParameterizedIsCalled1TimeWhenReceivingExceptionForWhichItShouldNotRetry(){
        given().contentType(APPLICATION_JSON)
               .params(Map.of("httpStatus", "400"))
               .expect()
               .statusCode(SERVICE_UNAVAILABLE.value())
               .when()
               .get(BASE_PATH + port + "/retry/statuscodeexception");

        List<ILoggingEvent> logsList = listAppender.list;
        assertThat(logsList).isNotEmpty();
        assertThat(logsList).hasSize(1);
        assertThat(logsList.get(0).getLevel()).isEqualTo(Level.INFO);
    }

    @Test
    void retryIsNotTriggeredAtAllForNonErrorStatusCode(){
        Response response = given().contentType(APPLICATION_JSON)
                                   .params(Map.of("httpStatus", "200"))
                                   .expect()
                                   .statusCode(OK.value())
                                   .when()
                                   .get(BASE_PATH + port + "/retry/statuscodeexception");

        assertThat(response.getBody().asString()).contains("No error");
    }

    private static List<Level> extractTriggeredLogsList(List<ILoggingEvent> logsList) {
        return logsList.stream()
                       .map(ILoggingEvent::getLevel)
                       .distinct()
                       .collect(Collectors.toList());
    }
}
