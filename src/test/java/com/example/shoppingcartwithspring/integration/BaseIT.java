package com.example.shoppingcartwithspring.integration;

import com.example.shoppingcartwithspring.model.dto.request.ItemWrapperRequestDto;
import io.restassured.http.ContentType;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.fail;
import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;

@RunWith(SpringRunner.class)
@ActiveProfiles("integration-test")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@ContextConfiguration(initializers = {SharedPostgreSQLContainer.Initializer.class})
public abstract class BaseIT {

    public static final String BASE_PATH = "http://localhost:";
    public static final String APPLICATION_JSON = "application/json";
    public static final String USERNAME_KEY = "username";
    public static final String PASSWORD_KEY = "password";
    public static final String ADMIN_USERNAME = "emil94";
    public static final String ADMIN_PASSWORD = "admin";
    public static final String USER_USERNAME = "marta95";
    public static final String USER_PASSWORD = "user";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String API_LOGIN = "/api/login";
    protected static final Network SHARED_NETWORK = Network.newNetwork();
    protected static final PostgreSQLContainer<?> pgContainer = SharedPostgreSQLContainer.createContainer();

    protected static Headers adminAuthorizationHeader;

    protected static Headers userAuthorizationHeader;

    @LocalServerPort
    private int port;

    static {
        startContainers(pgContainer);
    }

    private static void startContainers(GenericContainer<?>... containers) {
        for (GenericContainer<?> container : containers) {
            container.start();
        }
    }

    @BeforeAll
    public void setUp() {
        init();
    }

    private void init() {
        try {
            adminAuthorizationHeader = putTokenOnHeadersAndReturnIt(getAdminAccessToken());
            userAuthorizationHeader = putTokenOnHeadersAndReturnIt(getUserAccessToken());
        } catch (Exception e) {
            fail("Failed to execute prerequisite", e);
        }
    }

    public Response postForAddingItemInCart(String name,
                                            String quantity,
                                            String cartId) {

        ItemWrapperRequestDto itemWrapperRequestDto = new ItemWrapperRequestDto();
        itemWrapperRequestDto.setQuantity(quantity);
        itemWrapperRequestDto.setName(name);

        Headers headers = putTokenOnHeadersAndReturnIt(getAdminAccessToken());

        return given()
                .contentType(ContentType.JSON)
                .headers(headers)
                .body(itemWrapperRequestDto)
                .expect()
                .statusCode(HttpStatus.OK.value())
                .when()
                .post(BASE_PATH + port + "/cart/" + cartId + "/additem");
    }

    private String getAdminAccessToken() {
        return given()
                .contentType(APPLICATION_FORM_URLENCODED_VALUE)
                .formParam(USERNAME_KEY, ADMIN_USERNAME)
                .formParam(PASSWORD_KEY, ADMIN_PASSWORD)
                .when()
                .post(BASE_PATH + port + API_LOGIN)
                .jsonPath()
                .get(ACCESS_TOKEN);
    }

    private String getUserAccessToken() {
        return given()
                .contentType(APPLICATION_FORM_URLENCODED_VALUE)
                .formParam(USERNAME_KEY, USER_USERNAME)
                .formParam(PASSWORD_KEY, USER_PASSWORD)
                .when()
                .post(BASE_PATH + port + API_LOGIN)
                .jsonPath()
                .get(ACCESS_TOKEN);
    }

    public Headers putTokenOnHeadersAndReturnIt(String token) {
        Header authorization = new Header("Authorization", "Bearer " + token);
        return new Headers(authorization);
    }
}
