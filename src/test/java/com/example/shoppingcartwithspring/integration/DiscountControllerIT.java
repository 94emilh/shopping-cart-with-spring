package com.example.shoppingcartwithspring.integration;

import com.example.shoppingcartwithspring.audit.service.AuditService;
import com.example.shoppingcartwithspring.model.dto.request.DiscountRequestDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.model.entity.DiscountEntity;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;
import com.example.shoppingcartwithspring.repository.CartRepository;
import com.example.shoppingcartwithspring.repository.DiscountRepository;
import com.example.shoppingcartwithspring.repository.ItemRepository;
import com.example.shoppingcartwithspring.service.CartService;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.provider.Arguments;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.testcontainers.shaded.org.apache.commons.lang3.StringUtils;

import java.util.stream.Stream;

import static com.example.shoppingcartwithspring.audit.model.ActionType.APPLY_DISCOUNT_TO_CART;
import static com.example.shoppingcartwithspring.audit.model.ActionType.CREATE_DISCOUNT;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.DISCOUNT_COULD_NOT_BE_APPLIED_TO_CART_DUE_TO_INVALID_CART_ID;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.DISCOUNT_DESCRIPTION_BETWEEN_1_AND_30_CHARS;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.DISCOUNT_PERCENTAGE_BETWEEN_0_AND_50_PERCENT;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.DISCOUNT_PERCENTAGE_SHOULD_BE_A_NUMBER;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.MULTIPLE_DISCOUNTS_WITH_SIMILAR_DESCRIPTION_EXIST;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NAME_NOT_BLANK_OR_NULL;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NO_DISCOUNT_WITH_GIVEN_DESCRIPTION_FOUND;
import static com.example.shoppingcartwithspring.mock.ItemMock.APPLE_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.BANANA_ITEM;
import static com.example.shoppingcartwithspring.mock.ItemMock.BEER_ITEM;
import static io.restassured.RestAssured.given;
import static java.lang.String.valueOf;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.FORBIDDEN;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.OK;

class DiscountControllerIT extends BaseIT {

    public static final String BF = "BF";
    public static final String BF_DESCRIPTION = "Black Friday 30% Sale";
    @Autowired
    private CartService cartService;

    @Autowired
    private AuditService auditService;

    @Autowired
    private DiscountRepository discountRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ItemRepository itemRepository;

    @LocalServerPort
    private int port;

    @BeforeEach
    void init() {
        cartRepository.deleteAll();
        itemRepository.deleteAll();
        auditService.deleteAll();
        discountRepository.deleteAll();
    }

    private static Stream<Arguments> isAdminStatusCodeCombination() {
        return Stream.of(Arguments.of(true, OK),
                         Arguments.of(false, FORBIDDEN));
    }

    @Test
    void creatingADiscountWorksAccordinglyIfCalledByAdmin() {
        //GIVEN
        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName("BF");
        discountRequestDto.setDescription("Black Friday 20% Sale");
        discountRequestDto.setDiscountPercentage(String.valueOf(20));

        //WHEN
        String responseBodyAsString = given().contentType(APPLICATION_JSON)
                                             .headers(adminAuthorizationHeader)
                                             .body(discountRequestDto)
                                             .expect()
                                             .statusCode(OK.value())
                                             .when()
                                             .post(BASE_PATH + port + "/discount/create").getBody().asString();

        //THEN
        assertThat(responseBodyAsString).contains("\"discountPercentage\":20.0");
        assertThat(discountRepository.findAll()).hasSize(1);
        DiscountEntity createdDiscount = discountRepository.findAll().get(0);
        assertThat(createdDiscount.getName()).isEqualTo(BF);
        assertThat(createdDiscount.getDescription()).isEqualTo("Black Friday 20% Sale");
        assertThat(createdDiscount.getDiscountPercentage()).isEqualTo(20);
        assertThat(auditService.findAll()).hasSize(1);
        assertThat(auditService.findAll().get(0).getActionType()).isEqualTo(CREATE_DISCOUNT);
    }

    @Test
    void creatingADiscountReturns403ForbiddenIfCalledByNormalUser() {
        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName("BF");
        discountRequestDto.setDescription("Black Friday 20% Sale");
        discountRequestDto.setDiscountPercentage(String.valueOf(20));

        //WHEN
        String responseBodyAsString = given().contentType(APPLICATION_JSON)
                                             .headers(userAuthorizationHeader)
                                             .body(discountRequestDto)
                                             .expect()
                                             .statusCode(FORBIDDEN.value())
                                             .when()
                                             .post(BASE_PATH + port + "/discount/create").getBody().asString();
    }

    @Test
    void findAllDiscountsReturnsAllExistingDiscounts() {
        postForCreatingDiscount(BF, BF_DESCRIPTION, 30);
        postForCreatingDiscount("CS", "Christmas 10% Sale", 10);

        assertThat(discountRepository.findAll()).hasSize(2);
        assertThat(discountRepository.findAll().get(0).getDiscountPercentage()).isEqualTo(30);
        assertThat(discountRepository.findAll().get(1).getDiscountPercentage()).isEqualTo(10);
        assertThat(auditService.findAll()).hasSize(2);
        assertThat(auditService.findAll().get(0).getActionType()).isEqualTo(CREATE_DISCOUNT);
        assertThat(auditService.findAll().get(1).getActionType()).isEqualTo(CREATE_DISCOUNT);
    }

    @Test
    void findAllDiscountsWithGivenDescriptionReturnsOnlyTheCorrectOnes() {
        //GIVEN
        postForCreatingDiscount(BF, BF_DESCRIPTION, 30);
        postForCreatingDiscount(BF, "Early Black Friday 15% Sale", 15);
        postForCreatingDiscount("CS", "Christmas 10% Sale", 10);

        //WHEN
        String responseBodyAsString = getAllExistingDiscountsWithGivenDescription("Black Friday", 200).getBody().asString();

        //THEN
        assertThat(responseBodyAsString).contains(BF_DESCRIPTION);
        assertThat(responseBodyAsString).contains("Early Black Friday 15% Sale");
        assertThat(responseBodyAsString).doesNotContain("Christmas 10% Sale");
    }

    @Test
    void findAllDiscountsWithNonExistingDescriptionThrowsException() {
        //GIVEN
        postForCreatingDiscount(BF, BF_DESCRIPTION, 30);
        postForCreatingDiscount(BF, "Early Black Friday 15% Sale", 15);
        postForCreatingDiscount("CS", "Christmas 10% Sale", 10);

        assertThat(getAllExistingDiscountsWithGivenDescription("Cyber Monday", 500).getBody().asString()).contains(
                "No discount with given description was found!");
    }


    @Test
    void whenDiscountIsAppliedToACartTheTotalAmountIsAdjustedAccordinglyIfCalledByAdmin() {
        //GIVEN
        long cartId = createCartAndItemsAndReturnCartId();
        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        //WHEN
        executePostsForAddingItemsInCart(cartId);

        //THEN
        assertThat(cartService.computeTotalToPay(cartId).getTotalAmountToPay()).isEqualTo(66);
        patchToApplyDiscountToCart(cartId, BF, BF_DESCRIPTION, 30, OK, adminAuthorizationHeader);
        assertThat(cartService.computeTotalToPayAfterDiscount(cartId).getTotalAmountToPay()).isEqualTo(46.2);
        assertThat(auditService.findAll()).hasSize(4); // 3 add_item_to_cart audit events and one apply_discount_to_cart audit event
        assertThat(auditService.findAll().get(3).getActionType()).isEqualTo(APPLY_DISCOUNT_TO_CART);
    }

    @Test
    void whenDiscountIsAppliedToACartReturns403ForbiddenIfCalledByUser() {
        //GIVEN
        long cartId = createCartAndItemsAndReturnCartId();
        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        //WHEN
        executePostsForAddingItemsInCart(cartId);

        //THEN
        assertThat(cartService.computeTotalToPay(cartId).getTotalAmountToPay()).isEqualTo(66);
        patchToApplyDiscountToCart(cartId, BF, BF_DESCRIPTION, 30, FORBIDDEN, userAuthorizationHeader);
    }

    @Test
    void whenDiscountWhichDoesNotExistIsAppliedToACartAnExceptionIsThrown() {
        //GIVEN
        long cartId = createCartAndItemsAndReturnCartId();

        //WHEN
        executePostsForAddingItemsInCart(cartId);

        //THEN
        Response applyDiscountResponse = patchToApplyDiscountToCart(cartId, BF, BF_DESCRIPTION, 30, INTERNAL_SERVER_ERROR, adminAuthorizationHeader);
        assertThat(applyDiscountResponse.getBody().asString()).contains(NO_DISCOUNT_WITH_GIVEN_DESCRIPTION_FOUND.getMessage());
    }

    @Test
    void whenMultipleDiscountsWithSimilarDescriptionAreFoundAndDiscountIsAppliedToACartAnExceptionIsThrown() {
        //GIVEN
        long cartId = createCartAndItemsAndReturnCartId();

        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));
        discountRepository.save(new DiscountEntity(BF, "Early Black Friday 15% Sale", 15));

        //WHEN
        executePostsForAddingItemsInCart(cartId);

        //THEN
        Response applyDiscountResponse = patchToApplyDiscountToCart(cartId, BF, "Black Friday", 30, INTERNAL_SERVER_ERROR, adminAuthorizationHeader);
        assertThat(applyDiscountResponse.getBody().asString()).contains(MULTIPLE_DISCOUNTS_WITH_SIMILAR_DESCRIPTION_EXIST.getMessage());
    }

    @Test
    void whenDiscountIsAppliedToANonExistingCartIdAnExceptionIsThrown() {
        //GIVEN
        long cartId = createCartAndItemsAndReturnCartId();
        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        //WHEN
        executePostsForAddingItemsInCart(cartId);
        long invalidCartId = cartId + 1;

        //THEN
        Response applyDiscountResponse = patchToApplyDiscountToCart(invalidCartId,
                                                                    BF,
                                                                    BF_DESCRIPTION,
                                                                    30,
                                                                    INTERNAL_SERVER_ERROR,
                                                                    adminAuthorizationHeader);
        assertThat(applyDiscountResponse.getBody().asString()).contains(DISCOUNT_COULD_NOT_BE_APPLIED_TO_CART_DUE_TO_INVALID_CART_ID.getMessageWithParams(
                invalidCartId));
    }

    @Test
    void creatingADiscountWithBlankNameThrowsException() {
        //GIVEN
        createCartAndItemsAndReturnCartId();

        //THEN
        Response createDiscountResponse = postForCreatingDiscountBadRequest("", BF_DESCRIPTION, String.valueOf(30));
        assertThat(createDiscountResponse.getBody().asString()).contains(NAME_NOT_BLANK_OR_NULL.getMessage());
    }

    @Test
    void creatingADiscountWithNegativePercentageThrowsException() {
        //WHEN
        createCartAndItemsAndReturnCartId();

        //THEN
        Response createDiscountResponse = postForCreatingDiscountBadRequest(BF, BF_DESCRIPTION, String.valueOf(-10));
        assertThat(createDiscountResponse.getBody().asString()).contains(DISCOUNT_PERCENTAGE_BETWEEN_0_AND_50_PERCENT.getMessage());
    }

    @Test
    void creatingADiscountWithNonNumberPercentageThrowsException() {
        //WHEN
        createCartAndItemsAndReturnCartId();

        //THEN
        Response createDiscountResponse = postForCreatingDiscountBadRequest(BF, BF_DESCRIPTION, "invalidPercentage");
        assertThat(createDiscountResponse.getBody().asString()).contains(DISCOUNT_PERCENTAGE_SHOULD_BE_A_NUMBER.getMessage());
    }

    @Test
    void creatingADiscountWithDescriptionLongerThanTheSizeLimitThrowsException() {
        //WHEN
        createCartAndItemsAndReturnCartId();
        String invalidDescription = StringUtils.repeat("*", 31);

        //THEN
        assertThat(invalidDescription.length()).isGreaterThan(30);
        Response createDiscountResponse = postForCreatingDiscountBadRequest(BF, invalidDescription, "30");
        assertThat(createDiscountResponse.getBody().asString()).contains(DISCOUNT_DESCRIPTION_BETWEEN_1_AND_30_CHARS.getMessage());
    }

    @Test
    void whenDiscountIsAppliedToItemTheAmountIsAdjustedAccordinglyIfCalledByAdmin() {
        //GIVEN
        long itemId = itemRepository.save(new ItemEntity(BANANA_ITEM, 10)).getId();
        assertThat(itemRepository.findAll().get(0).getPrice()).isEqualTo(10);
        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName(BF);
        discountRequestDto.setDescription(BF_DESCRIPTION);
        discountRequestDto.setDiscountPercentage(String.valueOf(30));

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body(discountRequestDto)
                .expect()
                .statusCode(OK.value())
                .when()
                .patch(BASE_PATH + port + "/discount/applytoitem/" + itemId);
    }

    @Test
    void whenDiscountIsAppliedToItemReturns403ForbiddenIfCalledByNormalUser() {
        //GIVEN
        long itemId = itemRepository.save(new ItemEntity(BANANA_ITEM, 10)).getId();
        assertThat(itemRepository.findAll().get(0).getPrice()).isEqualTo(10);
        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName(BF);
        discountRequestDto.setDescription(BF_DESCRIPTION);
        discountRequestDto.setDiscountPercentage(String.valueOf(30));

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(userAuthorizationHeader)
                .body(discountRequestDto)
                .expect()
                .statusCode(FORBIDDEN.value())
                .when()
                .patch(BASE_PATH + port + "/discount/applytoitem/" + itemId);
    }

    @Test
    void updateDiscountByIdWorksAccordinglyIfCalledByAdmin() {
        //GIVEN
        DiscountEntity existingDiscount = discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        assertThat(discountRepository.findAll().get(0).getDescription()).isEqualTo(BF_DESCRIPTION);

        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName(BF);
        discountRequestDto.setDescription("New Description");
        discountRequestDto.setDiscountPercentage(String.valueOf(30));

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body(discountRequestDto)
                .expect()
                .statusCode(OK.value())
                .when()
                .put(BASE_PATH + port + "/discount/updateById/" + existingDiscount.getDiscountId());
    }

    @Test
    void updateDiscountByIdWorksReturns403IfCalledByNormalUser() {
        //GIVEN
        DiscountEntity existingDiscount = discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        assertThat(discountRepository.findAll().get(0).getDescription()).isEqualTo(BF_DESCRIPTION);

        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName(BF);
        discountRequestDto.setDescription("New Description");
        discountRequestDto.setDiscountPercentage(String.valueOf(30));

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(userAuthorizationHeader)
                .body(discountRequestDto)
                .expect()
                .statusCode(FORBIDDEN.value())
                .when()
                .put(BASE_PATH + port + "/discount/updateById/" + existingDiscount.getDiscountId());
    }

    @Test
    void updateDiscountByDescriptionWorksAccordinglyIfCalledByAdmin() {
        //GIVEN
        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        assertThat(discountRepository.findAll().get(0).getDescription()).isEqualTo(BF_DESCRIPTION);

        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName(BF);
        discountRequestDto.setDescription("New Description");
        discountRequestDto.setDiscountPercentage(String.valueOf(30));

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .queryParam("description", BF_DESCRIPTION)
                .body(discountRequestDto)
                .expect()
                .statusCode(OK.value())
                .when()
                .put(BASE_PATH + port + "/discount/updateByDescription");

        assertThat(discountRepository.findAll().get(0).getDescription()).isEqualTo("New Description");
    }

    @Test
    void updateDiscountByDescriptionReturns403ForbiddenIfCalledByNormalUser() {
        //GIVEN
        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        assertThat(discountRepository.findAll().get(0).getDescription()).isEqualTo(BF_DESCRIPTION);

        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName(BF);
        discountRequestDto.setDescription("New Description");
        discountRequestDto.setDiscountPercentage(String.valueOf(30));

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(userAuthorizationHeader)
                .queryParam("description", BF_DESCRIPTION)
                .body(discountRequestDto)
                .expect()
                .statusCode(FORBIDDEN.value())
                .when()
                .put(BASE_PATH + port + "/discount/updateByDescription");
    }

    @Test
    void deleteDiscountByIdWorksAccordinglyIfCalledByAdmin() {
        //GIVEN
        DiscountEntity existingDiscount = discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        assertThat(discountRepository.findAll()).isNotEmpty();

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .expect()
                .statusCode(OK.value())
                .when()
                .delete(BASE_PATH + port + "/discount/deleteById/" + existingDiscount.getDiscountId());
        assertThat(discountRepository.findAll()).isEmpty();
    }

    @Test
    void deleteDiscountByIdReturns403ForbiddenIfCalledByNormalUser() {
        //GIVEN
        DiscountEntity existingDiscount = discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        assertThat(discountRepository.findAll()).isNotEmpty();

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(userAuthorizationHeader)
                .expect()
                .statusCode(FORBIDDEN.value())
                .when()
                .delete(BASE_PATH + port + "/discount/deleteById/" + existingDiscount.getDiscountId());
    }

    @Test
    void deleteDiscountByDescriptionWorksAccordinglyIfCalledByAdmin() {
        //GIVEN
        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        assertThat(discountRepository.findAll()).isNotEmpty();

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .queryParam("description", BF_DESCRIPTION)
                .expect()
                .statusCode(OK.value())
                .when()
                .delete(BASE_PATH + port + "/discount/deleteByDescription");
        assertThat(discountRepository.findAll()).isEmpty();
    }

    @Test
    void deleteDiscountByDescriptionReturns403ForbiddenIfCalledByNormalUser() {
        //GIVEN
        discountRepository.save(new DiscountEntity(BF, BF_DESCRIPTION, 30));

        assertThat(discountRepository.findAll()).isNotEmpty();

        //THEN
        given()
                .contentType(APPLICATION_JSON)
                .headers(userAuthorizationHeader)
                .queryParam("description", BF_DESCRIPTION)
                .expect()
                .statusCode(FORBIDDEN.value())
                .when()
                .delete(BASE_PATH + port + "/discount/deleteByDescription");
    }

    private long createCartAndItemsAndReturnCartId() {
        CartEntity cart = new CartEntity();
        cartRepository.save(cart);

        itemRepository.save(new ItemEntity(BANANA_ITEM, 5));
        itemRepository.save(new ItemEntity(APPLE_ITEM, 2));
        itemRepository.save(new ItemEntity(BEER_ITEM, 10));

        return cart.getCartId();
    }

    private Response postForCreatingDiscount(String name,
                                             String description,
                                             double discountPercentage) {
        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName(name);
        discountRequestDto.setDescription(description);
        discountRequestDto.setDiscountPercentage(String.valueOf(discountPercentage));

        return given().contentType(APPLICATION_JSON)
                      .headers(adminAuthorizationHeader)
                      .body(discountRequestDto)
                      .expect()
                      .statusCode(OK.value())
                      .when()
                      .post(BASE_PATH + port + "/discount/create");
    }

    private Response postForCreatingDiscountBadRequest(String name,
                                                       String description,
                                                       String discountPercentage) {
        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName(name);
        discountRequestDto.setDescription(description);
        discountRequestDto.setDiscountPercentage(discountPercentage);

        return given().contentType(APPLICATION_JSON)
                      .headers(adminAuthorizationHeader)
                      .body(discountRequestDto)
                      .expect()
                      .statusCode(BAD_REQUEST.value())
                      .when()
                      .post(BASE_PATH + port + "/discount/create");
    }

    private Response getAllExistingDiscountsWithGivenDescription(String description,
                                                                 int statusCode) {
        return given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .queryParam("description", description)
                .expect()
                .statusCode(statusCode)
                .when()
                .get(BASE_PATH + port + "/discount/getall");
    }

    private Response patchToApplyDiscountToCart(long cartId,
                                                String name,
                                                String description,
                                                double discountPercentage,
                                                HttpStatus statusCode,
                                                Headers headers) {
        DiscountRequestDto discountRequestDto = new DiscountRequestDto();
        discountRequestDto.setName(name);
        discountRequestDto.setDescription(description);
        discountRequestDto.setDiscountPercentage(String.valueOf(discountPercentage));

        return given()
                .contentType(APPLICATION_JSON)
                .headers(headers)
                .body(discountRequestDto)
                .expect()
                .statusCode(statusCode.value())
                .when()
                .patch(BASE_PATH + port + "/discount/applytocart/" + cartId);
    }

    private void executePostsForAddingItemsInCart(long cartId) {
        postForAddingItemInCart(BANANA_ITEM, "2", valueOf(cartId));
        postForAddingItemInCart(APPLE_ITEM, "3", valueOf(cartId));
        postForAddingItemInCart(BEER_ITEM, "5", valueOf(cartId));
    }
}
