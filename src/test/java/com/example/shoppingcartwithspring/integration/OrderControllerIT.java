package com.example.shoppingcartwithspring.integration;

import com.example.shoppingcartwithspring.mock.ItemMock;
import com.example.shoppingcartwithspring.model.dto.request.CreateOrderRequestDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemCartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;
import com.example.shoppingcartwithspring.model.entity.OrderEntity;
import com.example.shoppingcartwithspring.repository.CartRepository;
import com.example.shoppingcartwithspring.repository.ItemCartRepository;
import com.example.shoppingcartwithspring.repository.ItemRepository;
import com.example.shoppingcartwithspring.repository.OrderRepository;
import io.restassured.response.Response;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.util.Set;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.USER_ID_DOES_NOT_MATCH_WITH_AUTHENTICATED_USER;
import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.*;

class OrderControllerIT extends BaseIT {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ItemCartRepository itemCartRepository;

    @Autowired
    private ItemRepository itemRepository;

    @LocalServerPort
    private int port;

    @BeforeEach
    void init() {
        orderRepository.deleteAll();
        cartRepository.deleteAll();
        itemRepository.deleteAll();
        itemCartRepository.deleteAll();
    }

    @Test
    void createOrderWorksAsExpectedWhenCalledBySameUserIdAsTheOneFromTheToken() {
        //GIVEN
        CartEntity cartEntity = new CartEntity();
        cartRepository.save(cartEntity);
        ItemCartEntity item = new ItemCartEntity();
        ItemEntity actualItemToSave = ItemMock.getItemEntityWithNameAndPrice(ItemMock.BANANA_ITEM, 10);
        itemRepository.save(actualItemToSave);
        item.setItem(actualItemToSave);
        item.setQuantity(10);
        itemCartRepository.save(item);
        cartEntity.setItems(Set.of(item));
        cartEntity.setTotalAmountToPay(100.0);
        cartRepository.save(cartEntity);

        //WHEN
        Response response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body(new CreateOrderRequestDto(cartEntity.getCartId(), ADMIN_USERNAME))
                .expect()
                .statusCode(CREATED.value())
                .when()
                .post(BASE_PATH + port + "/order/create");

        //THEN
        assertThat(response.getBody().asString()).contains("\"totalPrice\":100.0");
        assertThat(orderRepository.findAll()).hasSize(1);
        assertThat(orderRepository.findAll().get(0).getTotalPrice()).isEqualTo(100.0);
    }

    @Test
    void createOrderReturnsForbiddenIfCalledByDifferentUserIdComparedWithTheOneFromTheToken() {
        //GIVEN
        CartEntity cartEntity = new CartEntity();
        cartRepository.save(cartEntity);
        ItemCartEntity item = new ItemCartEntity();
        ItemEntity actualItemToSave = ItemMock.getItemEntityWithNameAndPrice(ItemMock.BANANA_ITEM, 10);
        itemRepository.save(actualItemToSave);
        item.setItem(actualItemToSave);
        item.setQuantity(10);
        itemCartRepository.save(item);
        cartEntity.setItems(Set.of(item));
        cartEntity.setTotalAmountToPay(100.0);
        cartRepository.save(cartEntity);

        //WHEN
        Response response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body(new CreateOrderRequestDto(cartEntity.getCartId(), USER_USERNAME))
                .expect()
                .statusCode(FORBIDDEN.value())
                .when()
                .post(BASE_PATH + port + "/order/create");
        assertThat(response.getBody().asString()).contains(USER_ID_DOES_NOT_MATCH_WITH_AUTHENTICATED_USER.getMessage());
    }

    @Test
    void findOrderByIdWorksAsExpected() {
        // Prepare test data
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setTotalPrice(200.0);
        orderEntity.setOrderStatus("Pending");
        orderEntity.setUserId("user123");
        orderRepository.save(orderEntity);

        // Test retrieving order by ID
        Response response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .expect()
                .statusCode(OK.value())
                .when()
                .get(BASE_PATH + port + "/order/" + orderEntity.getOrderId());

        assertThat(response.getBody().asString()).contains("\"totalPrice\":200.0", "\"orderStatus\":\"Pending\"");
    }

    @Test
    void findOrderByIdReturns404ForNonExistingOrder() {
        // Try to fetch a non-existing order
        Response response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .expect()
                .statusCode(NOT_FOUND.value())
                .when()
                .get(BASE_PATH + port + "/order/999");

        assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND.value());
    }

    @Test
    void updateStatusWorksAsExpected() {
        // Prepare test data
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setTotalPrice(300.0);
        orderEntity.setOrderStatus("Pending");
        orderEntity.setUserId("user456");
        orderRepository.save(orderEntity);

        // Update order status
        Response response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body("Shipped")
                .expect()
                .statusCode(OK.value())
                .when()
                .put(BASE_PATH + port + "/order/" + orderEntity.getOrderId() + "/status");

        assertThat(response.getBody().asString()).contains("\"orderStatus\":\"Shipped\"");
        assertThat(orderRepository.findAll().get(0).getOrderStatus()).isEqualTo("Shipped");
    }

    @Test
    void updateOrderStatusReturns404ForNonExisting() {
        // Try to update the status of a non-existing order
        Response response = given()
                .contentType(APPLICATION_JSON)
                .headers(adminAuthorizationHeader)
                .body("Shipped")
                .expect()
                .statusCode(NOT_FOUND.value())
                .when()
                .put(BASE_PATH + port + "/order/999/status");

        assertThat(response.getStatusCode()).isEqualTo(NOT_FOUND.value());
    }
}
