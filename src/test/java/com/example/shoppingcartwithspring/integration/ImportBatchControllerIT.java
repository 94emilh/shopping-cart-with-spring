package com.example.shoppingcartwithspring.integration;

import com.example.shoppingcartwithspring.repository.DiscountRepository;
import io.restassured.http.ContentType;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.web.server.LocalServerPort;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.http.HttpStatus.OK;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

class ImportBatchControllerIT extends BaseIT {

    private static final String CSV_IMPORT_FILE_PATH = "src/test/resources/discounts.csv";

    @Autowired
    private DiscountRepository discountRepository;

    @LocalServerPort
    private int port;

    @BeforeEach
    void init() {
        discountRepository.deleteAll();
    }

    @Test
    @Disabled("for now disabled - I have excluded BatchAutoConfiguration class from main to stop triggering job at application start-up, which makes " +
            "the test fail for the moment")
    void csvReaderEndpointsActuallyReadsTheEntries() throws IOException {
        assertThat(discountRepository.findAll()).isEmpty();

        given().contentType(ContentType.JSON)
               .accept(APPLICATION_JSON_VALUE)
               .headers(adminAuthorizationHeader)
               .when()
               .body(Files.readAllBytes(Paths.get(CSV_IMPORT_FILE_PATH)))
               .post(BASE_PATH + port + "/import/batch")
               .then().assertThat().statusCode(OK.value());

        assertThat(discountRepository.findAll()).hasSize(100);
    }
}