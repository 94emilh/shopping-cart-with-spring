package com.example.shoppingcartwithspring.mock;

import com.example.shoppingcartwithspring.model.dto.DiscountDto;
import com.example.shoppingcartwithspring.model.entity.DiscountEntity;

public class DiscountMock {

    public static final String BF_DISCOUNT_NAME = "BF";

    public static DiscountEntity getDiscountEntityWithPercentage(double discountPercentage) {
        DiscountEntity discountEntity = new DiscountEntity();
        discountEntity.setName(BF_DISCOUNT_NAME);
        discountEntity.setDescription(discountPercentage + "% Black Friday sale");
        discountEntity.setDiscountPercentage(discountPercentage);
        return discountEntity;
    }

    public static DiscountDto getDiscountDtoWithDiscountPercentageParametrized(double discountPercentage) {
        DiscountDto discountDto = new DiscountDto();
        discountDto.setDiscountId(11L);
        discountDto.setName(BF_DISCOUNT_NAME);
        discountDto.setDescription(discountPercentage + "% Black Friday sale");
        discountDto.setDiscountPercentage(discountPercentage);
        return discountDto;
    }
}
