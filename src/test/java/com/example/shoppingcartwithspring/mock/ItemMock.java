package com.example.shoppingcartwithspring.mock;

import com.example.shoppingcartwithspring.model.dto.ItemDto;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;

public class ItemMock {

    public static final String BANANA_ITEM = "banana";

    public static final String APPLE_ITEM = "apple";

    public static final String BEER_ITEM = "beer";

    public static ItemEntity getItemEntityWithName(String name){
        return new ItemEntity(name, 10);
    }

    public static ItemEntity getItemEntityWithNameAndPrice(String name, double price){
        return new ItemEntity(name, price);
    }

    public static ItemDto getItemDtoWithName(String name){
        return new ItemDto(name, 10);
    }


}
