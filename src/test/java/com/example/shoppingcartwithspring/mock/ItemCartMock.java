package com.example.shoppingcartwithspring.mock;

import com.example.shoppingcartwithspring.model.entity.ItemCartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;

public class ItemCartMock {

    public static ItemCartEntity getItemCartEntityWithItemAndQuantityParametrized(ItemEntity item,
                                                                                  int quantity) {
        ItemCartEntity itemCartEntity = new ItemCartEntity();
        itemCartEntity.setItem(item);
        itemCartEntity.setQuantity(quantity);
        return itemCartEntity;
    }
}
