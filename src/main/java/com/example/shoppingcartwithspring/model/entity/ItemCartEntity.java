package com.example.shoppingcartwithspring.model.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

import static jakarta.persistence.CascadeType.MERGE;

@Entity(name = "ItemCart")
@Table(name = "itemCart")
@Data
public class ItemCartEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @OneToOne(cascade = MERGE)
    @JoinColumn(name = "item_id", referencedColumnName = "id")
    private ItemEntity item;

    private int quantity;
}
