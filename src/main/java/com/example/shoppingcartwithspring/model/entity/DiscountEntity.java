package com.example.shoppingcartwithspring.model.entity;

import lombok.Data;
import jakarta.persistence.*;

@Entity(name = "Discount")
@Table(name = "discount")
@Data
public class DiscountEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "discount_id")
    private long discountId;

    private String name;

    private String description;

    private double discountPercentage;

    public DiscountEntity(String name,
                          String description,
                          double discountPercentage) {
        this.name = name;
        this.description = description;
        this.discountPercentage = discountPercentage;
    }

    public DiscountEntity(){}
}
