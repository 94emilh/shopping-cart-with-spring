package com.example.shoppingcartwithspring.model.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

import static jakarta.persistence.CascadeType.ALL;

@Entity(name = "Item")
@Table(name = "item")
@Data
public class ItemEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private String name;
    private double price;

    @OneToOne(cascade = ALL)
    @JoinColumn(name = "discount_id")
    private DiscountEntity discountEntity;

    public ItemEntity(String name,
                      double price) {
        this.name = name;
        this.price = price;
    }

    public ItemEntity() {
    }
}
