package com.example.shoppingcartwithspring.model.entity;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.OneToOne;
import jakarta.persistence.Table;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Entity(name = "Cart")
@Table(name = "cart")
@Data
public class CartEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "cart_id")
    private long cartId;

    @OneToOne(cascade = jakarta.persistence.CascadeType.ALL)
    @JoinColumn(name = "discount_id")
    private DiscountEntity discount;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "cart_id")
    private Set<ItemCartEntity> items = new HashSet<>();

    private double totalAmountToPay;

    public CartEntity(DiscountEntity discount) {
        this.discount = discount;
    }

    public CartEntity(){}

    @Override
    public String toString() {
        return "CartEntity{" +
                "cartId=" + cartId +
                ", discount=" + discount.getDiscountPercentage() +
                ", totalAmountToPay=" + totalAmountToPay +
                '}';
    }

    public void addNewItemCart(ItemCartEntity itemCartEntity){
        items.add(itemCartEntity);
    }
}
