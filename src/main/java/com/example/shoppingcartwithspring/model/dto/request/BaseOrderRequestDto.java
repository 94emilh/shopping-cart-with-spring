package com.example.shoppingcartwithspring.model.dto.request;

import com.example.shoppingcartwithspring.validation.MatchingUserIdConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public abstract class BaseOrderRequestDto {

    private Long cartId;

    @MatchingUserIdConstraint
    private String userId;
}
