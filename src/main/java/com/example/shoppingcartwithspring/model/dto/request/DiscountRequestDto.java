package com.example.shoppingcartwithspring.model.dto.request;

import com.example.shoppingcartwithspring.validation.DescriptionLengthLimitsConstraint;
import com.example.shoppingcartwithspring.validation.NameNotBlankOrNullConstraint;
import com.example.shoppingcartwithspring.validation.PercentageLimitsConstraint;
import org.springframework.validation.annotation.Validated;

@Validated
public class DiscountRequestDto {
    @NameNotBlankOrNullConstraint
    private String name;
    @DescriptionLengthLimitsConstraint
    private String description;
    @PercentageLimitsConstraint
    private String discountPercentage;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(String discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    @Override
    public String toString() {
        return "DiscountRequestDto{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", discountPercentage='" + discountPercentage + '\'' +
                '}';
    }
}
