package com.example.shoppingcartwithspring.model.dto;

import lombok.Data;

@Data
public class ItemCartDto {

    private Long itemId;

    private int quantity;

    public ItemCartDto(Long itemId,
                       int quantity) {
        this.itemId = itemId;
        this.quantity = quantity;
    }

    public ItemCartDto(){}
}
