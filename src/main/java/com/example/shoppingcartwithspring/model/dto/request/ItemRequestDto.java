package com.example.shoppingcartwithspring.model.dto.request;

import com.example.shoppingcartwithspring.validation.NameNotBlankOrNullConstraint;
import com.example.shoppingcartwithspring.validation.PriceNotNegativeAndNotNullConstraint;
import org.springframework.validation.annotation.Validated;

@Validated
public class ItemRequestDto {

    @NameNotBlankOrNullConstraint
    private String name;
    @PriceNotNegativeAndNotNullConstraint
    private String price;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "ItemRequestDto{" +
                "name='" + name + '\'' +
                ", price='" + price + '\'' +
                '}';
    }

}
