package com.example.shoppingcartwithspring.model.dto.request;

import com.example.shoppingcartwithspring.validation.NameNotBlankOrNullConstraint;
import com.example.shoppingcartwithspring.validation.QuantityNotNegativeAndNotNullConstraint;
import org.springframework.validation.annotation.Validated;

@Validated
public class ItemWrapperRequestDto {
    @NameNotBlankOrNullConstraint
    private String name;
    @QuantityNotNegativeAndNotNullConstraint
    private String quantity;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "ItemWrapperRequestDto{" +
                "name='" + name + '\'' +
                ", quantity=" + quantity +
                '}';
    }
}
