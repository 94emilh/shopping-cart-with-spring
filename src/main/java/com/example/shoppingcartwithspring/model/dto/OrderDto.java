package com.example.shoppingcartwithspring.model.dto;

import lombok.Data;

import java.util.Set;

@Data
public class OrderDto {
    private Long orderId;
    private Set<ItemCartDto> items;
    private double totalPrice;
    private String orderStatus;
    private String userId;
}
