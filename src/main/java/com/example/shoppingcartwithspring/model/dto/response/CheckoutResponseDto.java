package com.example.shoppingcartwithspring.model.dto.response;

import com.example.shoppingcartwithspring.model.dto.OrderDto;

public record CheckoutResponseDto(OrderDto order, String receipt) {
}
