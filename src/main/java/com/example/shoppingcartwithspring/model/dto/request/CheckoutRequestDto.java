package com.example.shoppingcartwithspring.model.dto.request;

import org.springframework.validation.annotation.Validated;

@Validated
public class CheckoutRequestDto extends BaseOrderRequestDto {

    public CheckoutRequestDto(Long cartId, String userId) {
        super(cartId, userId);
    }

}
