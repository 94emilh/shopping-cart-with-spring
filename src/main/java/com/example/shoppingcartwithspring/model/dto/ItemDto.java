package com.example.shoppingcartwithspring.model.dto;

import lombok.Data;

@Data
public class ItemDto {

    private long id;
    private String name;
    private double price;

    private Long discountId;

    public ItemDto(String name,
                   double price) {
        this.name = name;
        this.price = price;
    }

    public ItemDto(){};
}
