package com.example.shoppingcartwithspring.model.dto;

import lombok.Data;

@Data
public class DiscountDto {

    private long discountId;

    private String name;

    private String description;

    private double discountPercentage;

    public DiscountDto(String name,
                       String description,
                       double discountPercentage) {
        this.name = name;
        this.description = description;
        this.discountPercentage = discountPercentage;
    }

    public DiscountDto(){}
}
