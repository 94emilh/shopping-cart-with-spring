package com.example.shoppingcartwithspring.model.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Set;

@Data
public class CartDto implements Serializable {

    private long cartId;
    private DiscountDto discount;

    private Set<ItemCartDto> items;

    private double totalAmountToPay = 0;

    public CartDto(DiscountDto discount) {
        this.discount = discount;
    }

    public CartDto() {
    }
}
