package com.example.shoppingcartwithspring.model.dto;

import lombok.Data;

@Data
public class ItemWrapperDto {

    private long id;

    private String name;

    private int quantity;


    private DiscountDto discountDto;

    public ItemWrapperDto(String name,
                          int quantity) {
        this.name = name;
        this.quantity = quantity;
    }

    public ItemWrapperDto() {
    }

    ;
}
