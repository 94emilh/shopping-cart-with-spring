package com.example.shoppingcartwithspring.model.dto.request;


public class CreateOrderRequestDto extends BaseOrderRequestDto {

    public CreateOrderRequestDto(Long cartId, String userId) {
        super(cartId, userId);
    }
}
