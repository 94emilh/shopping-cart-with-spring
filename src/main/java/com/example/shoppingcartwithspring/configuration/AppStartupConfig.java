package com.example.shoppingcartwithspring.configuration;

import com.example.shoppingcartwithspring.model.entity.Role;
import com.example.shoppingcartwithspring.model.entity.User;
import com.example.shoppingcartwithspring.service.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;

@Configuration
public class AppStartupConfig {

    @Bean
    CommandLineRunner run(UserService userService) {
        return args -> {
            userService.saveRole(new Role(null, "user"));
            userService.saveRole(new Role(null,"admin"));

            userService.saveUser(new User(null, "Emil", "emil94", "admin", new ArrayList<>()));
            userService.saveUser(new User(null, "Marta", "marta95", "user", new ArrayList<>()));

            userService.addRoleToUser("emil94", "admin");
            userService.addRoleToUser("marta95", "user");
        };
    }
}
