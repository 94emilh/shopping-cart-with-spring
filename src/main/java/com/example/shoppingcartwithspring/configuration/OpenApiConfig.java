package com.example.shoppingcartwithspring.configuration;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI customOpenAPI() {

        return new OpenAPI()
                .info(new Info()
                              .title("Shopping cart with spring documentation")
                              .version("v0")
                              .description("This is the API for Shopping Cart with Spring Project by Emil.")
                              .termsOfService(null)
                              .license(null));
    }
}
