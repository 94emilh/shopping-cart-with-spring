package com.example.shoppingcartwithspring.configuration;

import com.example.shoppingcartwithspring.configuration.batch.DiscountProcessor;
import com.example.shoppingcartwithspring.configuration.batch.JobCompletionNotificationListener;
import com.example.shoppingcartwithspring.model.entity.DiscountEntity;
import lombok.AllArgsConstructor;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.job.builder.JobBuilder;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.core.repository.support.JobRepositoryFactoryBean;
import org.springframework.batch.core.step.builder.StepBuilder;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.core.task.TaskExecutor;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;

import javax.sql.DataSource;

@Configuration
@EnableBatchProcessing
@AllArgsConstructor
public class CsvBatchConfig {

    private final DataSource dataSource;

    @Bean
    @StepScope
    public FlatFileItemReader<DiscountEntity> discountReader(@Value("#{jobParameters['filePath']}") String filePath) {
        return new FlatFileItemReaderBuilder<DiscountEntity>()
                .name("discountReader")
                .resource(new FileSystemResource(filePath))
                .linesToSkip(1)
                .delimited()
                .names("discountId", "name", "description", "discountPercentage")
                .fieldSetMapper(new BeanWrapperFieldSetMapper<>() {{
                    setTargetType(DiscountEntity.class);
                }})
                .build();
    }

    @Bean
    public DiscountProcessor discountProcessor() {
        return new DiscountProcessor();
    }

    @Bean
    public JdbcBatchItemWriter<DiscountEntity> discountWriter(DataSource dataSource) {
        return new JdbcBatchItemWriterBuilder<DiscountEntity>()
                .sql("INSERT INTO discount (discount_id, description, discount_percentage, name) VALUES (:discountId, :description, :discountPercentage, :name)")
                .dataSource(dataSource)
                .beanMapped()
                .build();
    }

    @Bean
    public Step step1(FlatFileItemReader<DiscountEntity> reader,
                      DiscountProcessor processor,
                      JdbcBatchItemWriter<DiscountEntity> writer) throws Exception {
        return new StepBuilder("step1")
                .<DiscountEntity, DiscountEntity>chunk(5)
                .reader(reader)
                .processor(processor)
                .writer(writer)
                .repository(jobRepository())
                .transactionManager(new DataSourceTransactionManager(dataSource))
                .build();
    }

    @Bean
    public Job importUserJob(Step step1,
                             JobCompletionNotificationListener listener) throws Exception {
        return new JobBuilder("discount-job")
                .listener(listener)
                .repository(jobRepository())
                .start(step1)
                .build();
    }

    @Bean
    public JobRepository jobRepository() throws Exception {
        JobRepositoryFactoryBean factory = new JobRepositoryFactoryBean();
        factory.setDataSource(dataSource);
        factory.setTransactionManager(new DataSourceTransactionManager(dataSource));
        factory.setIsolationLevelForCreate("ISOLATION_DEFAULT");
        factory.afterPropertiesSet();
        return factory.getObject();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        return new SimpleAsyncTaskExecutor("spring_batch");
    }
}
