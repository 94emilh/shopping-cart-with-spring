package com.example.shoppingcartwithspring.configuration.batch;

import com.example.shoppingcartwithspring.model.entity.DiscountEntity;
import org.springframework.batch.item.ItemProcessor;

public class DiscountProcessor implements ItemProcessor<DiscountEntity, DiscountEntity> {

    @Override
    public DiscountEntity process(DiscountEntity discountEntity) {
        discountEntity.setName(discountEntity.getName() + " updated V2");
        return discountEntity;
    }
}
