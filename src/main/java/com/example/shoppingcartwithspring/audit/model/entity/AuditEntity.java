package com.example.shoppingcartwithspring.audit.model.entity;

import com.example.shoppingcartwithspring.audit.model.ActionType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import java.time.LocalDateTime;

@Entity(name = "Audit")
@Table(name = "audit")
@Data
public class AuditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
    private long cartId;
    private long itemId;
    private String itemName;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private ActionType actionType;
    @CreatedDate
    @Column(updatable = false, nullable = false)
    private LocalDateTime creationDate;


}
