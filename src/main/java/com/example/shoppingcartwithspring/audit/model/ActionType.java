package com.example.shoppingcartwithspring.audit.model;

public enum ActionType {
    INITIATE_CART,
    ADD_ITEM_TO_CART,

    CREATE_DISCOUNT,

    APPLY_DISCOUNT_TO_CART,

    CHECKOUT
}
