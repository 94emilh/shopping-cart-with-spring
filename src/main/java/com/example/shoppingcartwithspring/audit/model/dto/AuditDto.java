package com.example.shoppingcartwithspring.audit.model.dto;

import com.example.shoppingcartwithspring.audit.model.ActionType;
import lombok.Data;

import java.time.LocalDateTime;

@Data
public class AuditDto {

    private long id;
    private long cartId;
    private long itemId;
    private String itemName;
    private ActionType actionType;
    private LocalDateTime creationDate;
}
