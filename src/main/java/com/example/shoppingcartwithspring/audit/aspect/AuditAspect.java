package com.example.shoppingcartwithspring.audit.aspect;

import com.example.shoppingcartwithspring.audit.model.dto.AuditDto;
import com.example.shoppingcartwithspring.audit.service.AuditService;
import com.example.shoppingcartwithspring.model.dto.CartDto;
import com.example.shoppingcartwithspring.model.dto.DiscountDto;
import com.example.shoppingcartwithspring.model.dto.request.CheckoutRequestDto;
import com.example.shoppingcartwithspring.model.dto.request.DiscountRequestDto;
import com.example.shoppingcartwithspring.model.dto.ItemWrapperDto;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDateTime;

import static com.example.shoppingcartwithspring.audit.model.ActionType.ADD_ITEM_TO_CART;
import static com.example.shoppingcartwithspring.audit.model.ActionType.APPLY_DISCOUNT_TO_CART;
import static com.example.shoppingcartwithspring.audit.model.ActionType.CHECKOUT;
import static com.example.shoppingcartwithspring.audit.model.ActionType.CREATE_DISCOUNT;
import static com.example.shoppingcartwithspring.audit.model.ActionType.INITIATE_CART;

@Aspect
@Slf4j
public class AuditAspect {

    @Autowired
    private AuditService auditService;

    @After("execution( public * com.example.shoppingcartwithspring.service.CartService.create(..))")
    public void auditInitiateCart(JoinPoint joinPoint) {
        long cartId = extractCartIdFromJoinPointArg(joinPoint);
        log.info("Audit Initiate Cart: Cart with id {} has been created.", cartId);

        insertAuditRecordForCartCreation(joinPoint);
    }

    @After("execution( public * com.example.shoppingcartwithspring.service.CartService.addItemInCart(..))")
    public void auditAddItemToCart(JoinPoint joinPoint) {
        long itemId = extractItemIdFromJoinPointArg(joinPoint);
        log.info("Audit Add Item To Cart: Item with id {} has been added.", itemId);

        insertAuditRecordForItemCreation(joinPoint);
    }

    @After("execution( public * com.example.shoppingcartwithspring.service.CheckoutService.checkout(..))")
    public void auditCheckOut(JoinPoint joinPoint) {
        CheckoutRequestDto requestDto = (CheckoutRequestDto) joinPoint.getArgs()[0];
        var cartId = requestDto.getCartId();
        log.info("Audit Checkout: Checkout has been initiated for cart with id {}.", cartId);

        insertAuditRecordForCheckout(cartId);
    }

    @After("execution( public * com.example.shoppingcartwithspring.service.DiscountService.create(..))")
    public void auditCreateDiscount(JoinPoint joinPoint) {
        long discountId = extractDiscountIdFromJoinPointArg(joinPoint);
        log.info("Audit Create Discount: Discount with id {} has been added.", discountId);

        insertAuditRecordForDiscountCreation(joinPoint);
    }

    @After("execution( public * com.example.shoppingcartwithspring.service.DiscountService.applyDiscountToCart(..))")
    public void auditApplyDiscountToCart(JoinPoint joinPoint) {
        String description = extractDiscountDescriptionFromJoinPointArg(joinPoint);
        long cartId = extractCartIdFromJoinPointArgOfDiscount(joinPoint);
        log.info("Audit Apply Discount: Discount with description {} has been applied to cart with id {}.",
                 description,
                 cartId);

        insertAuditRecordForDiscountApply(cartId);
    }

    private void insertAuditRecordForCheckout(Long cartId) {
        var auditDto = new AuditDto();
        auditDto.setCreationDate(LocalDateTime.now());
        auditDto.setActionType(CHECKOUT);
        auditDto.setCartId(cartId);
        auditService.create(auditDto);
    }

    private void insertAuditRecordForDiscountApply(Long cartId) {
        var auditDto = new AuditDto();
        auditDto.setCreationDate(LocalDateTime.now());
        auditDto.setActionType(APPLY_DISCOUNT_TO_CART);
        auditDto.setCartId(cartId);
        auditService.create(auditDto);
    }

    private void insertAuditRecordForItemCreation(JoinPoint joinPoint) {
        var auditDto = new AuditDto();
        auditDto.setCreationDate(LocalDateTime.now());
        auditDto.setActionType(ADD_ITEM_TO_CART);
        auditDto.setCartId(extractItemIdFromJoinPointArg(joinPoint));
        auditService.create(auditDto);
    }

    private void insertAuditRecordForDiscountCreation(JoinPoint joinPoint) {
        var auditDto = new AuditDto();
        auditDto.setCreationDate(LocalDateTime.now());
        auditDto.setActionType(CREATE_DISCOUNT);
        auditDto.setCartId(extractDiscountIdFromJoinPointArg(joinPoint));
        auditService.create(auditDto);
    }


    private void insertAuditRecordForCartCreation(JoinPoint joinPoint) {
        var auditDto = new AuditDto();
        auditDto.setCreationDate(LocalDateTime.now());
        auditDto.setActionType(INITIATE_CART);
        auditDto.setCartId(extractCartIdFromJoinPointArg(joinPoint));
        auditService.create(auditDto);
    }

    private long extractCartIdFromJoinPointArg(JoinPoint joinPoint) {
        return ((CartDto) joinPoint.getArgs()[0]).getCartId();
    }

    private long extractCartIdFromJoinPointArgOfDiscount(JoinPoint joinPoint) {
        return ((Long) joinPoint.getArgs()[1]);
    }

    private long extractItemIdFromJoinPointArg(JoinPoint joinPoint) {
        return ((ItemWrapperDto) joinPoint.getArgs()[0]).getId();
    }

    private long extractDiscountIdFromJoinPointArg(JoinPoint joinPoint) {
        return ((DiscountDto) joinPoint.getArgs()[0]).getDiscountId();
    }

    private String extractDiscountDescriptionFromJoinPointArg(JoinPoint joinPoint) {
        return ((DiscountRequestDto) joinPoint.getArgs()[0]).getDescription();
    }


}
