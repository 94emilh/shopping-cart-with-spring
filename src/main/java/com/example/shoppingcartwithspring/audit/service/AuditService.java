package com.example.shoppingcartwithspring.audit.service;

import com.example.shoppingcartwithspring.audit.model.dto.AuditDto;
import com.example.shoppingcartwithspring.audit.model.entity.AuditEntity;
import com.example.shoppingcartwithspring.audit.repository.AuditRepository;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import org.springframework.transaction.annotation.Transactional;
import java.util.List;

import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class AuditService {

    private final ModelMapper mapper;
    private final AuditRepository auditRepository;

    public AuditService(ModelMapper mapper,
                        AuditRepository auditRepository) {
        this.mapper = mapper;
        this.auditRepository = auditRepository;
    }

    public AuditDto create(AuditDto auditDto) {
        var entity = mapper.map(auditDto, AuditEntity.class);
        return mapper.map(auditRepository.save(entity), AuditDto.class);
    }

    public List<AuditDto> findAll() {
        var auditEvents = auditRepository.findAll();
        if (auditEvents.isEmpty()) {
            return emptyList();
        }
        return auditEvents.stream().map(entity -> mapper.map(entity, AuditDto.class)).collect(toList());
    }

    public void deleteAll() {
        auditRepository.deleteAll();
    }
}
