package com.example.shoppingcartwithspring.audit.repository;

import com.example.shoppingcartwithspring.audit.model.entity.AuditEntity;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AuditRepository extends JpaRepository<AuditEntity, Long> {

    @Query("SELECT a.id,a.actionType,a.creationDate, a.cartId from Audit a")
    List<AuditEntity> findAllAuditEntriesForCart();

    @Query("SELECT a.id,a.actionType, a.creationDate, a.itemId,a.itemName,a.cartId from Audit a")
    List<AuditEntity> findAllAuditEntriesForItem();
}
