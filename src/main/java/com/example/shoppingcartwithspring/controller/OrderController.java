package com.example.shoppingcartwithspring.controller;

import com.example.shoppingcartwithspring.model.dto.OrderDto;
import com.example.shoppingcartwithspring.model.dto.request.CreateOrderRequestDto;
import com.example.shoppingcartwithspring.service.OrderService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/order", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class OrderController {

    private final OrderService orderService;


    @PostMapping("/create")
    public ResponseEntity<OrderDto> createOrder(@Valid @RequestBody
                                                CreateOrderRequestDto createOrderRequestDto) {
        try {
            OrderDto newOrder = orderService.createOrder(createOrderRequestDto);
            return ResponseEntity.status(201).body(newOrder);
        } catch (RuntimeException e) {
            return ResponseEntity.status(400).body(null);
        }
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrderDto> findOrderById(@PathVariable Long id) {
        try {
            OrderDto orderDto = orderService.findOrderById(id);
            return ResponseEntity.ok(orderDto);
        } catch (RuntimeException e) {
            return ResponseEntity.status(404).body(null);
        }
    }

    @PutMapping("/{id}/status")
    public ResponseEntity<OrderDto> updateStatus(@PathVariable Long id, @RequestBody String status) {
        try {
            OrderDto updatedOrder = orderService.updateStatus(id, status);
            return ResponseEntity.ok(updatedOrder);
        } catch (RuntimeException e) {
            return ResponseEntity.status(404).body(null);
        }
    }
}
