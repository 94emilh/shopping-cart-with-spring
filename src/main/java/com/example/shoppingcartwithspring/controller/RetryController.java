package com.example.shoppingcartwithspring.controller;

import com.example.shoppingcartwithspring.service.RetryService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/retry")
@AllArgsConstructor
public class RetryController {

    private final RetryService retryService;

    @GetMapping("/rte")
    public ResponseEntity<String> retryWithRuntimeException() {
            return ResponseEntity.ok(retryService.retryWithRuntimeException());
    }

    @GetMapping("/statuscodeexception")
    public ResponseEntity<String> retryWithHttpStatusCodeException(@RequestParam int httpStatus) {
            return ResponseEntity.ok(retryService.retryWithHttpStatusCodeException(httpStatus));
    }
}
