package com.example.shoppingcartwithspring.controller;

import com.example.shoppingcartwithspring.model.dto.request.CheckoutRequestDto;
import com.example.shoppingcartwithspring.model.dto.response.CheckoutResponseDto;
import com.example.shoppingcartwithspring.service.CheckoutService;
import jakarta.validation.Valid;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/checkout", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class CheckoutController {

    private final CheckoutService checkoutService;
    @PostMapping()
    public CheckoutResponseDto checkout(@Valid @RequestBody
                                        CheckoutRequestDto checkoutRequestDto) {
        return checkoutService.checkout(checkoutRequestDto);
    }
}
