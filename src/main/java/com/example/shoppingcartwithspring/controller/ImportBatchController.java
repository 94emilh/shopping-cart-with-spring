package com.example.shoppingcartwithspring.controller;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@RestController
@RequestMapping(path = "/import")
public class ImportBatchController {

    @Autowired
    private JobLauncher jobLauncher;

    @Autowired
    private Job job;

    @Autowired
    private Job importUserJob;

    @PostMapping("/batch")
    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<String> startCsvReaderJob(@RequestBody byte[] csv) {
        if (csv == null || csv.length == 0) {
            return ResponseEntity.badRequest().body("File is empty");
        }

        try {
            Path tempFilePath = Files.createTempFile("discounts-", ".csv");
            try (FileOutputStream fos = new FileOutputStream(tempFilePath.toFile())) {
                fos.write(csv);
            }

            JobParameters params = new JobParametersBuilder()
                    .addString("JobID", String.valueOf(System.currentTimeMillis()))
                    .addString("filePath", tempFilePath.toString())
                    .toJobParameters();
            jobLauncher.run(importUserJob, params);

            return ResponseEntity.ok("Batch job has been invoked");
        } catch (IOException e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Failed to save the file: " + e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(500).body("Failed to start batch job: " + e.getMessage());
        }
    }
}
