package com.example.shoppingcartwithspring.controller;

import com.example.shoppingcartwithspring.model.dto.DiscountDto;
import com.example.shoppingcartwithspring.model.dto.request.DiscountRequestDto;
import com.example.shoppingcartwithspring.service.DiscountService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/discount", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class DiscountController {

    private final DiscountService discountService;

    private final ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<DiscountDto>> retrieveAllDiscounts() {
        return ResponseEntity.ok(discountService.findAll());
    }

    @PostMapping("/create")
    public ResponseEntity<DiscountDto> create(@Valid @RequestBody DiscountRequestDto discountRequestDto) {
        DiscountDto discountDto = mapper.map(discountRequestDto, DiscountDto.class);
        return ResponseEntity.ok(discountService.create(discountDto));
    }

    @GetMapping("/getall")
    public ResponseEntity<List<DiscountDto>> retrieveAllDiscountsWithGivenDescription(@RequestParam(name = "description") String description) {
        return ResponseEntity.ok(discountService.findByDescription(description));
    }

    @PatchMapping("/applytocart/{cartId}")
    public String applyDiscountToCart(@Valid @RequestBody DiscountRequestDto discountDto,
                                      @PathVariable Long cartId) {
        return discountService.applyDiscountToCart(discountDto, cartId);
    }

    @PatchMapping("/applytoitem/{itemId}")
    public String applyDiscountToItem(@Valid @RequestBody DiscountRequestDto discountDto,
                                      @PathVariable Long itemId) {
        return discountService.applyDiscountToItem(discountDto, itemId);
    }

    @PutMapping("/updateById/{discountId}")
    public ResponseEntity<DiscountDto> updateById(@Valid @RequestBody DiscountRequestDto discountDto,
                                                          @PathVariable Long discountId) {
        return ResponseEntity.ok(discountService.updateDiscountById(discountDto, discountId));
    }

    @PutMapping("/updateByDescription")
    public ResponseEntity<DiscountDto> updateByDescription(@Valid @RequestBody DiscountRequestDto discountDto,
                                                                   @RequestParam(name = "description") String description) {
        return ResponseEntity.ok(discountService.updateDiscountByDescription(discountDto, description));
    }

    @DeleteMapping("/deleteById/{discountId}")
    public void deleteById(@PathVariable Long discountId) {
        discountService.deleteById(discountId);
    }

    @DeleteMapping("/deleteByDescription")
    public void deleteById(@RequestParam(name = "description") String description) {
        discountService.deleteByDescription(description);
    }
}
