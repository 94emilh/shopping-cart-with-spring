package com.example.shoppingcartwithspring.controller;

import com.example.shoppingcartwithspring.model.dto.ItemDto;
import com.example.shoppingcartwithspring.model.dto.request.ItemRequestDto;
import com.example.shoppingcartwithspring.service.ItemService;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import jakarta.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/item", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class ItemController {

    private final ModelMapper mapper;

    private final ItemService itemService;

    @GetMapping()
    public ResponseEntity<List<ItemDto>> retrieveAllItems() {
        return ResponseEntity.ok(itemService.findAll());
    }

    @GetMapping(path = "/{itemId}")
    public ResponseEntity<ItemDto> retrieveById(@PathVariable Long itemId) {
        return ResponseEntity.ok(itemService.findById(itemId));
    }

    @GetMapping("/getByName")
    public ResponseEntity<ItemDto> retrieveByName(@RequestParam(name = "itemName") String itemName) {
        return ResponseEntity.ok(itemService.findItemByName(itemName));
    }

    @PostMapping("/create")
    public ResponseEntity<ItemDto> create(@Valid @RequestBody ItemRequestDto itemRequestDto) {
        ItemDto itemDto = mapper.map(itemRequestDto, ItemDto.class);
        return ResponseEntity.ok(itemService.create(itemDto));
    }

    @PatchMapping("/updateprice/{itemName}")
    public ResponseEntity<ItemDto> updatePrice(@RequestParam(name = "price") String price,
                                               @PathVariable String itemName) {
        return ResponseEntity.ok(itemService.updatePriceForGivenItem(itemName, Double.parseDouble(price)));
    }
}
