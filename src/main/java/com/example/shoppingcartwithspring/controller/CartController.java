package com.example.shoppingcartwithspring.controller;

import com.example.shoppingcartwithspring.model.dto.CartDto;
import com.example.shoppingcartwithspring.model.dto.ItemWrapperDto;
import com.example.shoppingcartwithspring.model.dto.request.ItemWrapperRequestDto;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.example.shoppingcartwithspring.service.CartService;

import jakarta.validation.Valid;
import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(path = "/cart", produces = APPLICATION_JSON_VALUE)
@AllArgsConstructor
public class CartController {

    private final CartService cartService;
    private final ModelMapper mapper;

    @GetMapping
    public ResponseEntity<List<CartDto>> retrieveAllCarts() {
        return ResponseEntity.ok(cartService.findAll());
    }

    @GetMapping("/{cartId}")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Cart with given id was found and retrieved",
                         content = {@Content(mediaType = "application/json",
                                             schema = @Schema(implementation = CartDto.class))}),
            @ApiResponse(responseCode = "400", description = "Cart with given id was not found",
                         content = @Content(mediaType = "application/json"))})
    public ResponseEntity<CartDto> retrieveById(@PathVariable Long cartId) {
        return ResponseEntity.ok(cartService.findById(cartId));
    }

    @PostMapping("/create")
    public ResponseEntity<CartDto> create(@RequestBody(required = false) CartDto cartDto) {
        if(cartDto != null){
            return ResponseEntity.ok(cartService.create(cartDto));
        }
        return ResponseEntity.ok(cartService.create());
    }

    @PostMapping("/{cartId}/additem")
    public void addItemInCart(@PathVariable Long cartId,
                                                 @Valid @RequestBody
                                                 ItemWrapperRequestDto itemWrapperDto) {
        ItemWrapperDto itemDto = mapper.map(itemWrapperDto, ItemWrapperDto.class);
        cartService.addItemInCart(itemDto, cartId);
    }

    @DeleteMapping("/{cartId}")
    public void deleteCartById(@PathVariable Long cartId) {cartService.deleteCartById(cartId);}

    @PatchMapping("/deleteitems/{cartId}")
    public void deleteItemByNameFromGivenCart(@RequestParam(name = "itemName") String itemName, @PathVariable Long cartId){
        cartService.deleteItemByName(itemName, cartId);
    }
}
