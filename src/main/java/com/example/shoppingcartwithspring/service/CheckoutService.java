package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.exception.NoCartWithGivenIdWasFoundException;
import com.example.shoppingcartwithspring.model.dto.CartDto;
import com.example.shoppingcartwithspring.model.dto.ItemCartDto;
import com.example.shoppingcartwithspring.model.dto.OrderDto;
import com.example.shoppingcartwithspring.model.dto.request.CheckoutRequestDto;
import com.example.shoppingcartwithspring.model.dto.request.CreateOrderRequestDto;
import com.example.shoppingcartwithspring.model.dto.response.CheckoutResponseDto;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
@AllArgsConstructor
@Transactional
public class CheckoutService {

    private final CartService cartService;
    private final OrderService orderService;


    /**
     * Processes the checkout for a given cart. This method generates a receipt for the items in the cart
     * and creates a new order for the user.
     *
     * @param checkoutRequest A DTO containing the cart ID and user ID.
     * @return A {@link CheckoutResponseDto} containing the order details and the generated receipt.
     * @throws NoCartWithGivenIdWasFoundException if the provided cart ID is invalid.
     */
    public CheckoutResponseDto checkout(CheckoutRequestDto checkoutRequest) {
        CartDto cartDto = cartService.findById(checkoutRequest.getCartId());

        String receipt = buildReceipt(cartDto);

        OrderDto orderDto = orderService.createOrder(new CreateOrderRequestDto(checkoutRequest.getCartId(), checkoutRequest.getUserId()));
        return new CheckoutResponseDto(orderDto, receipt);
    }

    private String buildReceipt(CartDto cartDto) {
        long cartId = cartDto.getCartId();
        StringBuilder result = new StringBuilder();

        result.append("-----RECEIPT-----\n\n\n");
        Set<ItemCartDto> listOfItemsFromCartById = cartDto.getItems();

        for (ItemCartDto itemDto : listOfItemsFromCartById) {
            result.append("Item name: ").append(itemDto.getItemId())
                  .append("\n Quantity: ").append(itemDto.getQuantity())
                  .append("\n Value: ").append(itemDto.getQuantity() * 100)
                  .append("\n");
        }
        if(cartDto.getDiscount() != null) {
            result.append("\n------------------------\n")
                  .append("Total before discount: ").append(cartService.computeTotalToPay(cartId).getTotalAmountToPay())
                  .append("\nTotal after discount: ").append(cartService.computeTotalToPayAfterDiscount(cartId).getTotalAmountToPay());
        } else {
            result.append("\n------------------------\n")
                  .append("\nTotal:").append(cartService.computeTotalToPay(cartId));
        }
        return result.toString();
    }
}
