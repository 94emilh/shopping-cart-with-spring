package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.exception.NoCartWithGivenIdWasFoundException;
import com.example.shoppingcartwithspring.exception.NoItemWithGivenNameWasFoundException;
import com.example.shoppingcartwithspring.model.dto.CartDto;
import com.example.shoppingcartwithspring.model.dto.ItemDto;
import com.example.shoppingcartwithspring.model.dto.ItemWrapperDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemCartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;
import com.example.shoppingcartwithspring.repository.CartRepository;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NO_ITEM_WITH_GIVEN_NAME_WAS_FOUND;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
@AllArgsConstructor
public class CartService {

    private final ModelMapper mapper;
    private final CartRepository cartRepository;
    private final ItemService itemService;

    /**
     * @param cartDto -> creates a cart with given parameters and it
     * @return it
     */
    public CartDto create(CartDto cartDto) {
        CartEntity savedCart = cartRepository.save(mapper.map(cartDto, CartEntity.class));
        cartDto.setCartId(savedCart.getCartId());
        return mapper.map(savedCart, CartDto.class);
    }

    /**
     * @returns an empty cart
     */
    public CartDto create() {
        CartEntity savedCart = cartRepository.save(new CartEntity());
        return mapper.map(savedCart, CartDto.class);
    }

    /**
     * @return a list with all carts
     */
    public List<CartDto> findAll() {
        List<CartEntity> listOfCarts = cartRepository.findAll();
        if (listOfCarts.isEmpty()) {
            return emptyList();
        }
        return listOfCarts.stream()
                          .map(entity -> {
                              return mapper.map(entity, CartDto.class);
                          })
                          .collect(toList());
    }

    /**
     * @param id
     * @return cart with given id in case the given id exists, else
     * @throws NoCartWithGivenIdWasFoundException
     */
    public CartDto findById(Long id) {
        Optional<CartEntity> cartFoundById = cartRepository.findById(id);
        if (cartFoundById.isEmpty()) {
            throw new NoCartWithGivenIdWasFoundException("No cart with id " + id + " was found.");
        }
        return mapper.map(cartFoundById, CartDto.class);
    }

    /**
     * @param cartId         id of given cart
     * @param itemWrapperDto item to add in cart
     * @throws NoItemWithGivenNameWasFoundException in case no item with given name exists in the list of items
     */
    public void addItemInCart(ItemWrapperDto itemWrapperDto,
                              Long cartId) {
        Optional<CartEntity> currentCartOptional = cartRepository.findById(cartId);
        CartEntity cartEntity = currentCartOptional.get();

        String currentItemName = itemWrapperDto.getName();
        if (!itemService.itemAlreadyExists(currentItemName)) {
            throw new NoItemWithGivenNameWasFoundException(NO_ITEM_WITH_GIVEN_NAME_WAS_FOUND.getMessageWithParams(currentItemName));
        }

        ItemCartEntity newItem = new ItemCartEntity();
        ItemDto itemDto = itemService.findItemByName(itemWrapperDto.getName());
        newItem.setItem(mapper.map(itemDto, ItemEntity.class));
        newItem.setQuantity(itemWrapperDto.getQuantity());

        cartEntity.addNewItemCart(newItem);
    }

    /**
     * @param cartId   id of given cart
     * @param itemName Deletes item with given name(from both the general list of items and the belonging cart`s list of items)
     *                 in case the name exists or
     * @throws NoItemWithGivenNameWasFoundException in case no item with given name exists in given cart
     */
    @PreAuthorize("hasAuthority('admin')")
    public void deleteItemByName(String itemName,
                                 Long cartId) {
        Optional<CartEntity> cartFoundById = cartRepository.findById(cartId);
        if (cartFoundById.isPresent()) {
            List<ItemCartEntity> listOfItemsWithGivenName = cartFoundById.get().getItems().stream()
                                                                         .filter(c -> c.getItem().getName().equals(itemName))
                                                                         .toList();
            if (listOfItemsWithGivenName.isEmpty()) {
                throw new NoItemWithGivenNameWasFoundException("No items with name " + itemName + " were found in cart " +
                                                                       "with id " + cartId + ".");
            } else {
                Set<ItemCartEntity> setOfItems = new HashSet<>(cartFoundById.get().getItems());
                Iterator<ItemCartEntity> itemEntityIterator = setOfItems.iterator();
                while (itemEntityIterator.hasNext()) {
                    if (itemEntityIterator.next().getItem().getName().equals(itemName)) {
                        itemEntityIterator.remove();
                    }
                }
                cartFoundById.get().setItems(setOfItems);
            }
        }
    }

    /**
     * @param id Deletes cart with given id
     * @throws NoCartWithGivenIdWasFoundException in case the given id does not exist
     */
    @PreAuthorize("hasAuthority('admin')")
    public void deleteCartById(Long id) {
        cartRepository.deleteById(id);
    }

    /**
     * @param id Computes the total of the cart before applying the discount percentage.
     * @throws NoCartWithGivenIdWasFoundException in case the given id does not exist
     */
    public CartDto computeTotalToPay(Long id) {
        Optional<CartEntity> currentCart = cartRepository.findById(id);
        double totalAmount = 0;
        if (currentCart.isPresent()) {
            Set<ItemCartEntity> setOfItems = currentCart.get().getItems();
            Map<ItemEntity, Integer> mapOfItemsAndQuantities = new HashMap<>();
            for (ItemCartEntity itemCart : setOfItems) {
                mapOfItemsAndQuantities.put(itemCart.getItem(), itemCart.getQuantity());
            }
            for (Map.Entry<ItemEntity, Integer> entry : mapOfItemsAndQuantities.entrySet()) {
                totalAmount += entry.getValue() * entry.getKey().getPrice();
            }
            currentCart.get().setTotalAmountToPay(totalAmount);
        }
        return mapper.map(currentCart, CartDto.class);
    }

    /**
     * @param id Computes the total of the cart after applying the discount percentage.
     * @throws NoCartWithGivenIdWasFoundException in case the given id does not exist
     */
    public CartDto computeTotalToPayAfterDiscount(Long id) {
        Optional<CartEntity> currentCart = cartRepository.findById(id);
        double totalBeforeDiscount = computeTotalToPay(id).getTotalAmountToPay();
        if (totalBeforeDiscount != 0) {
            CartEntity cartEntity = currentCart.get();
            double discountPercentage = cartEntity.getDiscount().getDiscountPercentage();
            cartEntity.setTotalAmountToPay(totalBeforeDiscount - (totalBeforeDiscount * discountPercentage / 100));
            return mapper.map(currentCart, CartDto.class);
        } else {
            throw new NoCartWithGivenIdWasFoundException("No cart with id " + id + " was found!");
        }
    }
}
