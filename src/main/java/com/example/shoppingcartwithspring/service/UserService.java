package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.model.entity.Role;
import com.example.shoppingcartwithspring.model.entity.User;

import java.util.List;

public interface UserService {

    User saveUser(User user);
    Role saveRole(Role role);
    void addRoleToUser(String username, String roleName);
    User getUser(String username);
    List<User> getUsers();
}
