package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.model.dto.OrderDto;
import com.example.shoppingcartwithspring.model.dto.request.CreateOrderRequestDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.model.entity.ItemCartEntity;
import com.example.shoppingcartwithspring.model.entity.OrderEntity;
import com.example.shoppingcartwithspring.repository.ItemCartRepository;
import com.example.shoppingcartwithspring.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@AllArgsConstructor
public class OrderService {

    private final OrderRepository orderRepository;
    private final CartService cartService;
    private final ModelMapper mapper;
    private final ItemCartRepository itemCartRepository;

    /**
     * Creates a new order based on the given cart ID and user ID. The items from the cart are added to the new order.
     * Additionally, the order status is set to "Pending" and the total price is calculated from the cart's total amount.
     *
     * @param requestDto The request body containing both the cart ID and the ID of the user who is placing the order.
     * @return An {@link OrderDto} object containing the details of the newly created order.
     * @throws RuntimeException if any item in the cart cannot be found.
     */
    public OrderDto createOrder(CreateOrderRequestDto requestDto) {

        CartEntity cartEntity = mapper.map(cartService.findById(requestDto.getCartId()), CartEntity.class);

        OrderEntity newOrder = new OrderEntity();
        Set<ItemCartEntity> items = cartEntity.getItems();

        Set<ItemCartEntity> attachedItems = items.stream()
                                                 .map(item -> itemCartRepository.findByItem_Id(item.getItem().getId())
                                                                                .orElseThrow(() -> new RuntimeException("Item not found")))
                                                 .collect(Collectors.toSet());

        newOrder.setItems(attachedItems);
        if(cartEntity.getTotalAmountToPay() == 0) {
            throw new RuntimeException("In order to create the order you need first to checkout!");
        }
        newOrder.setTotalPrice(cartEntity.getTotalAmountToPay());
        newOrder.setOrderStatus("Pending");
        newOrder.setUserId(requestDto.getUserId());

        // Save order
        OrderEntity savedOrder = orderRepository.save(newOrder);

        // Optionally publish an event to Kafka
        publishOrderCreatedEvent(savedOrder);

        return mapper.map(savedOrder, OrderDto.class);
    }

    /**
     * Retrieves an order by its ID.
     *
     * @param orderId The ID of the order to retrieve.
     * @return An {@link OrderDto} object containing the order details.
     * @throws RuntimeException if the order is not found.
     */
    public OrderDto findOrderById(Long orderId) {
        Optional<OrderEntity> order = orderRepository.findById(orderId);
        if (order.isEmpty()) {
            throw new RuntimeException("Order not found");
        }
        return mapper.map(order.get(), OrderDto.class);
    }

    /**
     * Updates the status of an existing order.
     *
     * @param orderId The ID of the order to update.
     * @param status  The new status of the order.
     * @return An {@link OrderDto} object containing the updated order details.
     * @throws RuntimeException if the order is not found.
     */
    public OrderDto updateStatus(Long orderId, String status) {
        Optional<OrderEntity> order = orderRepository.findById(orderId);
        if (order.isEmpty()) {
            throw new RuntimeException("Order not found");
        }
        OrderEntity orderEntity = order.get();
        orderEntity.setOrderStatus(status);
        return mapper.map(orderRepository.save(orderEntity), OrderDto.class);
    }

    //TODO: Kafka Producer method to publish order creation event
    /**
     * Publishes an event when a new order is created (currently a placeholder for Kafka event publishing).
     *
     * @param orderEntity The newly created order entity to publish an event for.
     */
    private void publishOrderCreatedEvent(OrderEntity orderEntity) {
        // Publish order created event using Kafka (KafkaProducer logic here)
        // kafkaTemplate.send("order-events", new OrderCreatedEvent(orderEntity));
    }
}
