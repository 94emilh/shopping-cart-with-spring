package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.exception.ServiceUnavailableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Recover;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.HttpStatusCodeException;

@Service
public class RetryService {

    private static final Logger LOG = LoggerFactory.getLogger(RetryService.class);


    @Retryable(retryFor = RuntimeException.class, maxAttempts = 3, backoff = @Backoff(2000))
    public String retryWithRuntimeException() {
        LOG.info("calling retryWithRuntimeException endpoint");
        throw new ServiceUnavailableException("service not available");
    }

    @Retryable(retryFor = HttpStatusCodeException.class, maxAttempts = 3, backoff = @Backoff(2000), noRetryFor = HttpClientErrorException.class)
    public String retryWithHttpStatusCodeException(int httpStatus) {
        LOG.info("calling retryWithHttpStatusCodeException endpoint");
        if(String.valueOf(httpStatus).startsWith("5")) {
            throw new HttpServerErrorException(HttpStatus.valueOf(httpStatus));
        } else if (String.valueOf(httpStatus).startsWith("4")) {
            throw new HttpClientErrorException(HttpStatus.valueOf(httpStatus));
        }
        return "No error";
    }

    @Recover
    public String recover(HttpStatusCodeException exception){
        throw new ServiceUnavailableException("Please try after 10 minutes!");
    }
}
