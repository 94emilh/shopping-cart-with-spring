package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.exception.DiscountCouldNotBeAppliedException;
import com.example.shoppingcartwithspring.exception.DiscountsWithSimilarDescriptionExistException;
import com.example.shoppingcartwithspring.exception.NoDiscountWIthGivenDescriptionWasFound;
import com.example.shoppingcartwithspring.exception.NoDiscountWithGivenIdWasFound;
import com.example.shoppingcartwithspring.exception.NoDiscountWithSimilarDescriptionWasFoundException;
import com.example.shoppingcartwithspring.model.dto.DiscountDto;
import com.example.shoppingcartwithspring.model.dto.request.DiscountRequestDto;
import com.example.shoppingcartwithspring.model.entity.CartEntity;
import com.example.shoppingcartwithspring.model.entity.DiscountEntity;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;
import com.example.shoppingcartwithspring.repository.CartRepository;
import com.example.shoppingcartwithspring.repository.DiscountRepository;
import com.example.shoppingcartwithspring.repository.ItemRepository;
import jakarta.transaction.Transactional;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.DISCOUNT_COULD_NOT_BE_APPLIED_TO_CART_DUE_TO_INVALID_CART_ID;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.DISCOUNT_COULD_NOT_BE_APPLIED_TO_ITEM_DUE_TO_INVALID_ITEM_ID;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.MULTIPLE_DISCOUNTS_WITH_SIMILAR_DESCRIPTION_EXIST;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NO_DISCOUNT_WITH_GIVEN_DESCRIPTION_FOUND;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NO_DISCOUNT_WITH_GIVEN_ID_FOUND;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class DiscountService {

    private final ModelMapper mapper;
    private final DiscountRepository discountRepository;
    private final ItemRepository itemRepository;
    private final CartRepository cartRepository;

    public DiscountService(ModelMapper modelMapper,
                           DiscountRepository discountRepository,
                           ItemRepository itemRepository,
                           CartRepository cartRepository) {
        this.mapper = modelMapper;
        this.discountRepository = discountRepository;
        this.itemRepository = itemRepository;
        this.cartRepository = cartRepository;
    }

    /**
     * @param discountDto - creates a discount with given parameters and it
     * @return the created discount
     */
    @PreAuthorize("hasAuthority('admin')")
    public DiscountDto create(DiscountDto discountDto) {
        DiscountEntity savedDiscount = discountRepository.save(mapper.map(discountDto, DiscountEntity.class));
        discountDto.setDiscountId(savedDiscount.getDiscountId());
        return mapper.map(savedDiscount, DiscountDto.class);
    }

    /**
     * @return a list with all discounts
     */
    public List<DiscountDto> findAll() {
        List<DiscountEntity> listOfDiscounts = discountRepository.findAll();
        if (listOfDiscounts.isEmpty()) {
            return emptyList();
        }
        return mapListOfEntitiesToListOfDtos(listOfDiscounts);
    }

    /**
     * @param description
     * @return list of discounts which have a description containing the given param
     * @throws NoDiscountWithSimilarDescriptionWasFoundException
     */
    public List<DiscountDto> findByDescription(String description) {
        List<DiscountEntity> listOfDiscountsWithSimilarDescription = discountRepository.findAll().stream()
                                                                                       .filter(d -> d.getDescription().contains(description)).collect(
                        toList());
        if (!listOfDiscountsWithSimilarDescription.isEmpty()) {
            return mapListOfEntitiesToListOfDtos(listOfDiscountsWithSimilarDescription);
        } else {
            throw new NoDiscountWithSimilarDescriptionWasFoundException(NO_DISCOUNT_WITH_GIVEN_DESCRIPTION_FOUND.getMessage());
        }
    }

    /**
     * Applies discount to a given item(by id)
     * @param discountRequestDto - discount which will be applied
     * @param itemId - id of the item which will have the discount applied
     * @return String which contains a confirmation message
     * @throws DiscountCouldNotBeAppliedException in case no item with given id was found
     * @throws DiscountsWithSimilarDescriptionExistException in case there are multiple discounts with similar description
     */
    @PreAuthorize("hasAuthority('admin')")
    public String applyDiscountToItem(DiscountRequestDto discountRequestDto,
                                      Long itemId) {
        Optional<ItemEntity> itemFoundById = itemRepository.findById(itemId);
        List<DiscountDto> existingDiscountsWithGivenDescription = findByDescription(discountRequestDto.getDescription());

        if (existingDiscountsWithGivenDescription.size() > 1) {
            throw new DiscountsWithSimilarDescriptionExistException(MULTIPLE_DISCOUNTS_WITH_SIMILAR_DESCRIPTION_EXIST.getMessage());
        }
        if (itemFoundById.isPresent()) {
            ItemEntity item = itemFoundById.get();
            double itemPrice = item.getPrice();
            item.setDiscountEntity(mapper.map(discountRequestDto, DiscountEntity.class));
            item.setPrice(itemPrice - item.getDiscountEntity().getDiscountPercentage() / 100 * itemPrice);
            return "Discount of " + discountRequestDto.getDiscountPercentage() + "% has been applied to item " +
                    item.getName() + " within the " + discountRequestDto.getDescription() + " promotion.";
        } else {
            throw new DiscountCouldNotBeAppliedException(DISCOUNT_COULD_NOT_BE_APPLIED_TO_ITEM_DUE_TO_INVALID_ITEM_ID.getMessageWithParams(itemId));
        }
    }

    /**
     * Applies discount to a given cart(by id)
     * @param discountRequestDto - discount request object which will be applied
     * @param cartId - id of the cart which will have the discount applied
     * @return String which contains a confirmation message
     * @throws DiscountsWithSimilarDescriptionExistException in case there are multiple discounts with similar description
     * @throws DiscountCouldNotBeAppliedException in case no cart with given id was found
     */
    @PreAuthorize("hasAuthority('admin')")
    public String applyDiscountToCart(DiscountRequestDto discountRequestDto,
                                      Long cartId) {
        Optional<CartEntity> cartFoundById = cartRepository.findById(cartId);
        List<DiscountDto> existingDiscountsWithGivenDescription = findByDescription(discountRequestDto.getDescription());
        if (existingDiscountsWithGivenDescription.size() > 1) {
            throw new DiscountsWithSimilarDescriptionExistException(MULTIPLE_DISCOUNTS_WITH_SIMILAR_DESCRIPTION_EXIST.getMessage());
        }
        if (cartFoundById.isPresent()) {
            CartEntity cart = cartFoundById.get();
            cart.setDiscount(mapper.map(discountRequestDto, DiscountEntity.class));
            return "Discount of " + discountRequestDto.getDiscountPercentage() + "% has been applied to cart with id " +
                    cartId + " within the " + discountRequestDto.getDescription() + " promotion.";
        } else {
            throw new DiscountCouldNotBeAppliedException(DISCOUNT_COULD_NOT_BE_APPLIED_TO_CART_DUE_TO_INVALID_CART_ID.getMessageWithParams(cartId));
        }
    }

    /**
     * Updates existing discount(by id)
     *
     * @param discountRequestDto - discount request object which will be updated
     * @param discountId - id of the discount which will be updated
     * @return the updated DiscountDto object
     * @throws NoDiscountWithGivenIdWasFound in case there is no discount with given id
     */
    @PreAuthorize("hasAuthority('admin')")
    public DiscountDto updateDiscountById(DiscountRequestDto discountRequestDto,
                                          Long discountId) {
        Optional<DiscountEntity> discountOptional = discountRepository.findByDiscountId(discountId);
        if (discountOptional.isEmpty()) {
            throw new NoDiscountWithGivenIdWasFound(NO_DISCOUNT_WITH_GIVEN_ID_FOUND.getMessage());
        }
        DiscountEntity discountToBeUpdated = updateDiscount(discountOptional, mapper.map(discountRequestDto, DiscountDto.class));
        return mapper.map(discountToBeUpdated, DiscountDto.class);
    }

    /**
     * Updates existing discount(by id)
     *
     * @param discountRequestDto - discount request object which will be updated
     * @param description - description of the discount which will be updated
     * @return the updated DiscountDto object
     * @throws NoDiscountWithGivenIdWasFound in case there is no discount with given id
     * @throws DiscountsWithSimilarDescriptionExistException in case there are multiple descriptions which contain the received description
     */
    @PreAuthorize("hasAuthority('admin')")
    public DiscountDto updateDiscountByDescription(DiscountRequestDto discountRequestDto,
                                                   String description) {
        List<DiscountDto> listOfItemsContainingGivenDescription = findByDescription(description);
        if (listOfItemsContainingGivenDescription.isEmpty()) {
            throw new NoDiscountWIthGivenDescriptionWasFound(NO_DISCOUNT_WITH_GIVEN_DESCRIPTION_FOUND.getMessage());
        } else if (listOfItemsContainingGivenDescription.size() > 1) {
            throw new DiscountsWithSimilarDescriptionExistException(MULTIPLE_DISCOUNTS_WITH_SIMILAR_DESCRIPTION_EXIST.getMessage());
        }
        DiscountDto discountToBeUpdated = listOfItemsContainingGivenDescription.get(0);
        Optional<DiscountEntity> discountOptional = discountRepository.findByDiscountId(discountToBeUpdated.getDiscountId());
        DiscountEntity newDiscount = updateDiscount(discountOptional, mapper.map(discountRequestDto, DiscountDto.class));
        return mapper.map(newDiscount, DiscountDto.class);
    }

    /**
     * Deletes existing discount(by id)
     *
     * @param discountId - id of the discount which will be updated
     * @throws NoDiscountWithGivenIdWasFound in case there is no discount with given id
     */
    @PreAuthorize("hasAuthority('admin')")
    public void deleteById(Long discountId){
        Optional<DiscountEntity> discountOptional = discountRepository.findByDiscountId(discountId);
        if (discountOptional.isEmpty()) {
            throw new NoDiscountWithGivenIdWasFound(NO_DISCOUNT_WITH_GIVEN_ID_FOUND.getMessage());
        }
        discountRepository.delete(discountOptional.get());
    }

    /**
     * Deletes existing discount(by description)
     *
     * @param description - id of the discount which will be updated
     * @throws NoDiscountWIthGivenDescriptionWasFound in case there is no discount with given description
     * @throws DiscountsWithSimilarDescriptionExistException in case there are multiple descriptions which contain the received description
     */
    @PreAuthorize("hasAuthority('admin')")
    public void deleteByDescription(String description) {
        List<DiscountDto> listOfItemsContainingGivenDescription = findByDescription(description);
        if (listOfItemsContainingGivenDescription.isEmpty()) {
            throw new NoDiscountWIthGivenDescriptionWasFound(NO_DISCOUNT_WITH_GIVEN_DESCRIPTION_FOUND.getMessage());
        } else if (listOfItemsContainingGivenDescription.size() > 1) {
            throw new DiscountsWithSimilarDescriptionExistException(MULTIPLE_DISCOUNTS_WITH_SIMILAR_DESCRIPTION_EXIST.getMessage());
        }
        DiscountEntity existingDiscountToBeDeleted = mapper.map(listOfItemsContainingGivenDescription.get(0), DiscountEntity.class);
        discountRepository.delete(existingDiscountToBeDeleted);
    }

    private DiscountEntity updateDiscount(Optional<DiscountEntity> discountOptional,
                                          DiscountDto updatedDiscount) {
        DiscountEntity discountToBeUpdated = discountOptional.get();
        discountToBeUpdated.setDiscountPercentage(updatedDiscount.getDiscountPercentage());
        discountToBeUpdated.setDescription(updatedDiscount.getDescription());
        discountToBeUpdated.setName(updatedDiscount.getName());
        return discountToBeUpdated;
    }

    private List<DiscountDto> mapListOfEntitiesToListOfDtos(List<DiscountEntity> listOfDiscounts) {
        return listOfDiscounts.stream()
                              .map(entity -> {
                                  return mapper.map(entity, DiscountDto.class);
                              })
                              .collect(toList());
    }
}
