package com.example.shoppingcartwithspring.service;

import com.example.shoppingcartwithspring.exception.ItemWithGivenNameAlreadyExistsException;
import com.example.shoppingcartwithspring.exception.NoItemWithGivenIdWasFoundException;
import com.example.shoppingcartwithspring.exception.NoItemWithGivenNameWasFoundException;
import com.example.shoppingcartwithspring.model.dto.ItemDto;
import com.example.shoppingcartwithspring.model.entity.ItemEntity;
import com.example.shoppingcartwithspring.repository.ItemRepository;
import org.modelmapper.ModelMapper;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import jakarta.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.ITEM_NAME_ALREADY_EXISTS;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NO_ITEM_WITH_GIVEN_ID_WAS_FOUND;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.NO_ITEM_WITH_GIVEN_NAME_WAS_FOUND;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class ItemService {

    private final ModelMapper mapper;
    private final ItemRepository itemRepository;

    public ItemService(ModelMapper mapper,
                       ItemRepository itemRepository) {
        this.mapper = mapper;
        this.itemRepository = itemRepository;
    }

    /**
     * @param item -> creates an cart with given parameters and it
     * @return it
     * @throws ItemWithGivenNameAlreadyExistsException in case an item with same name already exists
     */
    @PreAuthorize("hasAuthority('admin')")
    public ItemDto create(ItemDto item) {
        ItemEntity itemEntity;
        String currentItemName = item.getName();
        if (itemAlreadyExists(currentItemName)) {
            throw new ItemWithGivenNameAlreadyExistsException(ITEM_NAME_ALREADY_EXISTS.getMessageWithParams(currentItemName));
        }
        itemEntity = itemRepository.save(mapper.map(item, ItemEntity.class));
        return mapper.map(itemEntity, ItemDto.class);
    }

    /**
     * @param id
     * @return item with given id in case it exists or
     * @throws NoItemWithGivenIdWasFoundException in case the given id does not exist
     */
    public ItemDto findById(long id) {
        Optional<ItemEntity> itemFoundById = itemRepository.findById(id);
        if (itemFoundById.isEmpty()) {
            throw new NoItemWithGivenIdWasFoundException(NO_ITEM_WITH_GIVEN_ID_WAS_FOUND.getMessageWithParams(id));
        }
        return mapper.map(itemRepository.findById(id), ItemDto.class);
    }

    /**
     * @return a list with all items
     */
    public List<ItemDto> findAll() {
        List<ItemEntity> listOfItems = itemRepository.findAll();
        if (listOfItems.isEmpty()) {
            return emptyList();
        }
        return listOfItems.stream()
                          .map(entity -> {
                              return mapper.map(entity, ItemDto.class);
                          })
                          .collect(toList());
    }

    /**
     * @param name
     * @return item with given name if it exists
     * @throws NoItemWithGivenNameWasFoundException in case the item with given name does not exist
     */
    public ItemDto findItemByName(String name) {
        ItemEntity itemEntityWithGivenName = itemRepository.findAll()
                                                           .stream()
                                                           .filter(i -> i.getName().equals(name))
                                                           .findFirst()
                                                           .orElseThrow(() -> new NoItemWithGivenNameWasFoundException(
                                                                   NO_ITEM_WITH_GIVEN_NAME_WAS_FOUND.getMessageWithParams(name)));
        return mapper.map(itemEntityWithGivenName, ItemDto.class);
    }

    /**
     * @param name
     * @param newPrice
     * @return item with updated price
     * @throws NoItemWithGivenNameWasFoundException in case the item with given name does not exist
     */
    @PreAuthorize("hasAuthority('admin')")
    public ItemDto updatePriceForGivenItem(String name,
                                           double newPrice) {
        ItemDto itemByName = findItemByName(name);
        if (itemByName != null) {
            itemByName.setPrice(newPrice);
        }
        return itemByName;
    }

    public boolean itemAlreadyExists(String name) {
        return itemRepository.findAll().stream().filter(i -> i.getName().equals(name)).count() == 1;
    }
}
