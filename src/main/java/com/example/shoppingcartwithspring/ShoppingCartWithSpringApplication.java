package com.example.shoppingcartwithspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.batch.BatchAutoConfiguration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableWebMvc
@SpringBootApplication(exclude = BatchAutoConfiguration.class)
@EnableAspectJAutoProxy
public class ShoppingCartWithSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingCartWithSpringApplication.class, args);
    }


}
