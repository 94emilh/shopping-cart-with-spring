package com.example.shoppingcartwithspring.exception;

public class NoItemWithGivenIdWasFoundException extends RuntimeException{

    private String name;

    public NoItemWithGivenIdWasFoundException(String name) {
        super(name);
    }
}
