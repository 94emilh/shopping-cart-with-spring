package com.example.shoppingcartwithspring.exception;

public class NoDiscountWithGivenIdWasFound extends RuntimeException{

    private String name;

    public NoDiscountWithGivenIdWasFound(String name) {
        super(name);
    }
}
