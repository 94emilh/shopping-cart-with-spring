package com.example.shoppingcartwithspring.exception;

public class NoItemWithGivenNameWasFoundException extends RuntimeException{

    private String name;

    public NoItemWithGivenNameWasFoundException(String name) {
        super(name);
    }
}
