package com.example.shoppingcartwithspring.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.FORBIDDEN)
public class UserIdMismatchException extends RuntimeException{

    public UserIdMismatchException(String message) {
        super(message);
    }
}
