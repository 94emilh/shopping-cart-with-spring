package com.example.shoppingcartwithspring.exception;

import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
public class ExceptionResponse {

    private final String path;
    private final String message;
}
