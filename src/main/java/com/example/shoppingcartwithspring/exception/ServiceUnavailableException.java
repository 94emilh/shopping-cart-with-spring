package com.example.shoppingcartwithspring.exception;

public class ServiceUnavailableException extends RuntimeException{

    private String name;

    public ServiceUnavailableException(String name) {
        super(name);
    }
}
