package com.example.shoppingcartwithspring.exception;

public class NoDiscountWithSimilarDescriptionWasFoundException extends RuntimeException{

    private String name;

    public NoDiscountWithSimilarDescriptionWasFoundException(String name) {
        super(name);
    }
}
