package com.example.shoppingcartwithspring.exception;

public class DiscountCouldNotBeAppliedException extends RuntimeException{

    private String name;

    public DiscountCouldNotBeAppliedException(String name) {
        super(name);
    }
}
