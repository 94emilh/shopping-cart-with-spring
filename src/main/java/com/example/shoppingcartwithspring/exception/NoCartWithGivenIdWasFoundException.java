package com.example.shoppingcartwithspring.exception;

public class NoCartWithGivenIdWasFoundException extends RuntimeException{

    private String name;

    public NoCartWithGivenIdWasFoundException(String name) {
        super(name);
    }
}
