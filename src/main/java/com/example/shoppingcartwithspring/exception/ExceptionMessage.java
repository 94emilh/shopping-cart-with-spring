package com.example.shoppingcartwithspring.exception;

public enum ExceptionMessage {

    NAME_NOT_BLANK_OR_NULL(101, "Item/Discount name should not be blank or null!"),
    PRICE_NOT_NEGATIVE_OR_NULL(102, "Price should not be negative or null!"),
    PRICE_SHOULD_BE_A_NUMBER(103, "Price should be a valid number!"),
    ITEM_NAME_ALREADY_EXISTS(104, "Item with name '%s' already exists!"),
    NO_ITEM_WITH_GIVEN_NAME_WAS_FOUND(105, "No item with name '%s' was found in the items list!"),
    NO_ITEM_WITH_GIVEN_ID_WAS_FOUND(106, "No item with id %s was found!"),
    QUANTITY_NOT_NEGATIVE_OR_NULL(107, "Quantity should not be negative or null!"),
    QUANTITY_SHOULD_BE_A_NUMBER(108, "Quantity should be a valid number!"),
    DISCOUNT_DESCRIPTION_BETWEEN_1_AND_30_CHARS(109, "Discount description should be between 1-100 characters!"),
    DISCOUNT_PERCENTAGE_BETWEEN_0_AND_50_PERCENT(110, "Discount percentage should be between 0 and 50%!"),
    DISCOUNT_PERCENTAGE_SHOULD_BE_A_NUMBER(111, "Discount percentage should be a valid number!"),
    NO_DISCOUNT_WITH_GIVEN_DESCRIPTION_FOUND(112, "No discount with given description was found!"),
    MULTIPLE_DISCOUNTS_WITH_SIMILAR_DESCRIPTION_EXIST(113, "There is more than one discount with similar description, please fill in " +
            "the exact description of the discount you want to apply!"),
    DISCOUNT_COULD_NOT_BE_APPLIED_TO_CART_DUE_TO_INVALID_CART_ID(114, "Discount could not be applied to cart because cart with id %s was not found!"),
    DISCOUNT_COULD_NOT_BE_APPLIED_TO_ITEM_DUE_TO_INVALID_ITEM_ID(115, "Discount could not be applied to item because item with id %s was not found!"),
    NO_DISCOUNT_WITH_GIVEN_ID_FOUND(116, "No discount with given id was found!"),
    USER_ID_DOES_NOT_MATCH_WITH_AUTHENTICATED_USER(117, "You are not authenticated or user ID does not match the authenticated user");

    private final String message;

    private Integer code;

    ExceptionMessage(final Integer code,
                     final String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getMessageWithParams(final Object... values) {
        return String.format(message, values);
    }
}
