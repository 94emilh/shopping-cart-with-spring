package com.example.shoppingcartwithspring.exception;

public class DiscountsWithSimilarDescriptionExistException extends RuntimeException{

    private String name;

    public DiscountsWithSimilarDescriptionExistException(String name) {
        super(name);
    }
}
