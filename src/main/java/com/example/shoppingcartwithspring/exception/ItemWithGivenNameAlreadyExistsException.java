package com.example.shoppingcartwithspring.exception;

public class ItemWithGivenNameAlreadyExistsException extends RuntimeException {

    private String name;

    public ItemWithGivenNameAlreadyExistsException(String name) {
        super(name);
    }
}
