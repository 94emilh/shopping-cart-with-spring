package com.example.shoppingcartwithspring.exception.exceptionhandler;

import com.example.shoppingcartwithspring.exception.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import jakarta.servlet.http.HttpServletRequest;
import java.util.Objects;

@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(NoItemWithGivenNameWasFoundException.class)
    public ResponseEntity<ExceptionResponse> handleNoItemWithGivenNameWasFoundException(NoItemWithGivenNameWasFoundException exception,
                                                                                        HttpServletRequest request) {
        return returnExceptionResponsePageFromRequest(exception, request);
    }

    @ExceptionHandler(NoDiscountWithSimilarDescriptionWasFoundException.class)
    public ResponseEntity<ExceptionResponse> handleNoDiscountWithSimilarDescriptionWasFoundException(NoDiscountWithSimilarDescriptionWasFoundException exception,
                                                                                                     HttpServletRequest request) {
        return returnExceptionResponsePageFromRequest(exception, request);
    }

    @ExceptionHandler(DiscountCouldNotBeAppliedException.class)
    public ResponseEntity<ExceptionResponse> handleDiscountCouldNotBeAppliedException(DiscountCouldNotBeAppliedException exception,
                                                                                      HttpServletRequest request) {
        return returnExceptionResponsePageFromRequest(exception, request);
    }

    @ExceptionHandler(DiscountsWithSimilarDescriptionExistException.class)
    public ResponseEntity<ExceptionResponse> handleDiscountsWithSimilarDescriptionExistException(DiscountsWithSimilarDescriptionExistException exception,
                                                                                                 HttpServletRequest request) {
        return returnExceptionResponsePageFromRequest(exception, request);
    }

    @ExceptionHandler(ItemWithGivenNameAlreadyExistsException.class)
    public ResponseEntity<ExceptionResponse> handleItemWithGivenNameAlreadyExistsException(ItemWithGivenNameAlreadyExistsException exception,
                                                                                           HttpServletRequest request) {
        return returnExceptionResponsePageFromRequest(exception, request);
    }

    @ExceptionHandler(ServiceUnavailableException.class)
    public ResponseEntity<ExceptionResponse> handleServiceUnavailableException(ServiceUnavailableException exception,
                                                                                           HttpServletRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(request.getServletPath(),
                                                                    exception.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @ExceptionHandler(UserIdMismatchException.class)
    public ResponseEntity<Object> handleUserIdMismatchException(UserIdMismatchException ex) {
        return new ResponseEntity<>(ex.getMessage(), HttpStatus.FORBIDDEN);
    }

    @Override
    protected ResponseEntity<Object> handleExceptionInternal(Exception exception,
                                                             Object body,
                                                             HttpHeaders headers,
                                                             HttpStatusCode status,
                                                             WebRequest request) {
        var apiError = Objects.isNull(body)
                ? exception.getMessage()
                : body;
        return super.handleExceptionInternal(exception, apiError, headers, status, request);
    }

    private ResponseEntity<ExceptionResponse> returnExceptionResponsePageFromRequest(RuntimeException exception,
                                                                                     HttpServletRequest request) {
        ExceptionResponse exceptionResponse = new ExceptionResponse(request.getServletPath(),
                                                                    exception.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
