package com.example.shoppingcartwithspring.repository;

import com.example.shoppingcartwithspring.model.entity.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);
}
