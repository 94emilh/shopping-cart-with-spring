package com.example.shoppingcartwithspring.repository;

import com.example.shoppingcartwithspring.model.entity.CartEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface CartRepository extends JpaRepository<CartEntity, Long> {

}
