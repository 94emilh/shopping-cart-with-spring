package com.example.shoppingcartwithspring.repository;

import com.example.shoppingcartwithspring.model.entity.DiscountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DiscountRepository extends JpaRepository<DiscountEntity, Long> {

    Optional<DiscountEntity> findByDiscountId(Long discountId);
}
