package com.example.shoppingcartwithspring.repository;

import com.example.shoppingcartwithspring.model.entity.ItemCartEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ItemCartRepository extends JpaRepository<ItemCartEntity, Long> {

    Optional<ItemCartEntity> findByItem_Id(Long itemId);
}
