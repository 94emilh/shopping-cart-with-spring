package com.example.shoppingcartwithspring.repository;

import com.example.shoppingcartwithspring.model.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {

    User findByUsername(String username);
}
