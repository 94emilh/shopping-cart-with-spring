package com.example.shoppingcartwithspring.repository;

import com.example.shoppingcartwithspring.model.entity.ItemEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ItemRepository extends JpaRepository<ItemEntity, Long> {
}
