package com.example.shoppingcartwithspring.repository;

import com.example.shoppingcartwithspring.model.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderRepository extends JpaRepository<OrderEntity, Long> {
}
