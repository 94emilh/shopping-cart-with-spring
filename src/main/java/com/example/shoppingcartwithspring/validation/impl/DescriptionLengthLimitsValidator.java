package com.example.shoppingcartwithspring.validation.impl;

import com.example.shoppingcartwithspring.validation.DescriptionLengthLimitsConstraint;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.*;

public class DescriptionLengthLimitsValidator extends AbstractValidator
        implements ConstraintValidator<DescriptionLengthLimitsConstraint, String> {

    @Override
    public boolean isValid(String description,
                           ConstraintValidatorContext context) {
        if(description == null || description.isBlank() || description.length() > 30) {
            disableDefaultConstraintAndBuildCustomOne(context, DISCOUNT_DESCRIPTION_BETWEEN_1_AND_30_CHARS.getMessage());
            return false;
        }
        return true;
    }
}
