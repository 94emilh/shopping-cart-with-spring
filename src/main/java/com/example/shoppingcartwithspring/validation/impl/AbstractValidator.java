package com.example.shoppingcartwithspring.validation.impl;

import jakarta.validation.ConstraintValidatorContext;

public abstract class AbstractValidator {

    public void disableDefaultConstraintAndBuildCustomOne(ConstraintValidatorContext context,
                                                          String message) {
        context.disableDefaultConstraintViolation();
        context.buildConstraintViolationWithTemplate(message).addConstraintViolation();
    }
}
