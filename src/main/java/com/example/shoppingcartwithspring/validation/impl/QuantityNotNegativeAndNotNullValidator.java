package com.example.shoppingcartwithspring.validation.impl;

import com.example.shoppingcartwithspring.validation.QuantityNotNegativeAndNotNullConstraint;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.QUANTITY_NOT_NEGATIVE_OR_NULL;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.QUANTITY_SHOULD_BE_A_NUMBER;

public class QuantityNotNegativeAndNotNullValidator extends AbstractValidator
        implements ConstraintValidator<QuantityNotNegativeAndNotNullConstraint, String> {

    @Override
    public boolean isValid(String value,
                           ConstraintValidatorContext context) {
        try {
            if (value == null || Integer.parseInt(value) <= 0) {
                disableDefaultConstraintAndBuildCustomOne(context, QUANTITY_NOT_NEGATIVE_OR_NULL.getMessage());
                return false;
            }
        } catch (NumberFormatException nfe) {
            disableDefaultConstraintAndBuildCustomOne(context, QUANTITY_SHOULD_BE_A_NUMBER.getMessage());
            return false;
        }
        return true;
    }
}
