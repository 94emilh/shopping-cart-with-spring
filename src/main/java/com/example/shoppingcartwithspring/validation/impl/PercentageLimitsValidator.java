package com.example.shoppingcartwithspring.validation.impl;

import com.example.shoppingcartwithspring.validation.PercentageLimitsConstraint;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.DISCOUNT_PERCENTAGE_BETWEEN_0_AND_50_PERCENT;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.DISCOUNT_PERCENTAGE_SHOULD_BE_A_NUMBER;

public class PercentageLimitsValidator extends AbstractValidator
        implements ConstraintValidator<PercentageLimitsConstraint, String> {

    @Override
    public boolean isValid(String discountPercentage,
                           ConstraintValidatorContext context) {
        try {
            if (discountPercentage == null || Double.parseDouble(discountPercentage) <= 0 || Double.parseDouble(discountPercentage) >= 50) {
                disableDefaultConstraintAndBuildCustomOne(context, DISCOUNT_PERCENTAGE_BETWEEN_0_AND_50_PERCENT.getMessage());
                return false;
            }
        } catch (NumberFormatException nfe) {
            disableDefaultConstraintAndBuildCustomOne(context, DISCOUNT_PERCENTAGE_SHOULD_BE_A_NUMBER.getMessage());
            return false;
        }
        return true;

    }
}
