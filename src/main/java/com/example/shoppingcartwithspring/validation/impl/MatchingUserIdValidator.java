package com.example.shoppingcartwithspring.validation.impl;

import com.example.shoppingcartwithspring.exception.UserIdMismatchException;
import com.example.shoppingcartwithspring.validation.MatchingUserIdConstraint;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.USER_ID_DOES_NOT_MATCH_WITH_AUTHENTICATED_USER;

public class MatchingUserIdValidator extends AbstractValidator implements ConstraintValidator<MatchingUserIdConstraint, String> {

    @Override
    public boolean isValid(String userId, ConstraintValidatorContext context) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || authentication.getPrincipal() == null) {
            throw new UserIdMismatchException(USER_ID_DOES_NOT_MATCH_WITH_AUTHENTICATED_USER.getMessage());
        }
        String loggedInUserId = getUserIdFromToken(authentication.getPrincipal());
        boolean isUserLoggedInAndHasTheSameUserId = loggedInUserId != null && loggedInUserId.equals(userId);

        if(!isUserLoggedInAndHasTheSameUserId) {
            throw new UserIdMismatchException(USER_ID_DOES_NOT_MATCH_WITH_AUTHENTICATED_USER.getMessage());
        }
        return true;
    }

    private String getUserIdFromToken(Object principal) {
        if (principal instanceof UserDetails userDetails) {
            return userDetails.getUsername();
        } else if (principal instanceof String string) {
            return string;
        }
        return null;
    }
}
