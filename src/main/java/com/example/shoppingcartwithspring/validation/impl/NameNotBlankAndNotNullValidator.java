package com.example.shoppingcartwithspring.validation.impl;

import com.example.shoppingcartwithspring.exception.ExceptionMessage;
import com.example.shoppingcartwithspring.validation.NameNotBlankOrNullConstraint;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.*;

public class NameNotBlankAndNotNullValidator extends AbstractValidator
        implements ConstraintValidator<NameNotBlankOrNullConstraint, String> {

    @Override
    public boolean isValid(String value,
                           ConstraintValidatorContext context) {
        if (value == null || value.isBlank()) {
            disableDefaultConstraintAndBuildCustomOne(context, NAME_NOT_BLANK_OR_NULL.getMessage());
            return false;
        }
        return true;
    }
}
