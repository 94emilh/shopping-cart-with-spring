package com.example.shoppingcartwithspring.validation.impl;

import com.example.shoppingcartwithspring.validation.PriceNotNegativeAndNotNullConstraint;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import static com.example.shoppingcartwithspring.exception.ExceptionMessage.PRICE_NOT_NEGATIVE_OR_NULL;
import static com.example.shoppingcartwithspring.exception.ExceptionMessage.PRICE_SHOULD_BE_A_NUMBER;

public class PriceNotNegativeAndNotNullValidator extends AbstractValidator
        implements ConstraintValidator<PriceNotNegativeAndNotNullConstraint, String> {

    @Override
    public boolean isValid(String value,
                           ConstraintValidatorContext context) {
        try {
            if (value == null || Double.parseDouble(value) <= 0) {
                disableDefaultConstraintAndBuildCustomOne(context, PRICE_NOT_NEGATIVE_OR_NULL.getMessage());
                return false;
            }
        } catch (NumberFormatException nfe) {
            disableDefaultConstraintAndBuildCustomOne(context, PRICE_SHOULD_BE_A_NUMBER.getMessage());
            return false;
        }
        return true;
    }
}
