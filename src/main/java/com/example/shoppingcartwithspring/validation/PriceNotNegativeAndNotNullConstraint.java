package com.example.shoppingcartwithspring.validation;

import com.example.shoppingcartwithspring.exception.ExceptionMessage;
import com.example.shoppingcartwithspring.validation.impl.PriceNotNegativeAndNotNullValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = PriceNotNegativeAndNotNullValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RUNTIME)
public @interface PriceNotNegativeAndNotNullConstraint {

    String message() default "";

    Class<?>[] groups() default {};

    ExceptionMessage code() default ExceptionMessage.PRICE_NOT_NEGATIVE_OR_NULL;

    Class<? extends Payload>[] payload() default {};
}
