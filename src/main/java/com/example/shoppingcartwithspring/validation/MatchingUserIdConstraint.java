package com.example.shoppingcartwithspring.validation;

import com.example.shoppingcartwithspring.exception.ExceptionMessage;
import com.example.shoppingcartwithspring.validation.impl.MatchingUserIdValidator;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = MatchingUserIdValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RUNTIME)
public @interface MatchingUserIdConstraint {

    String message() default "";
    Class<?>[] groups() default {};
    ExceptionMessage code() default ExceptionMessage.USER_ID_DOES_NOT_MATCH_WITH_AUTHENTICATED_USER;
    Class<? extends Payload>[] payload() default {};
}
