package com.example.shoppingcartwithspring.validation;

import com.example.shoppingcartwithspring.exception.ExceptionMessage;
import com.example.shoppingcartwithspring.validation.impl.NameNotBlankAndNotNullValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = NameNotBlankAndNotNullValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RUNTIME)
public @interface NameNotBlankOrNullConstraint {

    String message() default "";

    Class<?>[] groups() default {};

    ExceptionMessage code() default ExceptionMessage.NAME_NOT_BLANK_OR_NULL;

    Class<? extends Payload>[] payload() default {};
}
