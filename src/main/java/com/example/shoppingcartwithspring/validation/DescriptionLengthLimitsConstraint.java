package com.example.shoppingcartwithspring.validation;

import com.example.shoppingcartwithspring.exception.ExceptionMessage;
import com.example.shoppingcartwithspring.validation.impl.DescriptionLengthLimitsValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = DescriptionLengthLimitsValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RUNTIME)
public @interface DescriptionLengthLimitsConstraint {

    String message() default "";

    Class<?>[] groups() default {};

    ExceptionMessage code() default ExceptionMessage.DISCOUNT_DESCRIPTION_BETWEEN_1_AND_30_CHARS;

    Class<? extends Payload>[] payload() default {};
}
