package com.example.shoppingcartwithspring.validation;

import com.example.shoppingcartwithspring.exception.ExceptionMessage;
import com.example.shoppingcartwithspring.validation.impl.PercentageLimitsValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = PercentageLimitsValidator.class)
@Target({ElementType.FIELD, ElementType.PARAMETER})
@Retention(RUNTIME)
public @interface PercentageLimitsConstraint {

    String message() default "";

    Class<?>[] groups() default {};

    ExceptionMessage code() default ExceptionMessage.DISCOUNT_PERCENTAGE_BETWEEN_0_AND_50_PERCENT;

    Class<? extends Payload>[] payload() default {};
}
