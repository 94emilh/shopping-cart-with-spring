package com.example.shoppingcartwithspring.validation;

import com.example.shoppingcartwithspring.exception.ExceptionMessage;
import com.example.shoppingcartwithspring.validation.impl.QuantityNotNegativeAndNotNullValidator;

import jakarta.validation.Constraint;
import jakarta.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Documented
@Constraint(validatedBy = QuantityNotNegativeAndNotNullValidator.class)
@Target({ElementType.FIELD})
@Retention(RUNTIME)
public @interface QuantityNotNegativeAndNotNullConstraint {

    String message() default "";

    Class<?>[] groups() default {};

    ExceptionMessage code() default ExceptionMessage.QUANTITY_NOT_NEGATIVE_OR_NULL;

    Class<? extends Payload>[] payload() default {};
}
